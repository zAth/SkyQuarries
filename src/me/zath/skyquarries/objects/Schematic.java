package me.zath.skyquarries.objects;
/*
 * MC 
 * Created by zAth
 */

import org.bukkit.Location;
import org.bukkit.block.Block;

public class Schematic {

    private byte[] blocks;
    private byte[] data;
    private short width;
    private short length;
    private short height;

    public Schematic(byte[] blocks, byte[] data, short width, short length, short height) {
        this.blocks = blocks;
        this.data = data;
        this.width = width;
        this.length = length;
        this.height = height;
    }

    public byte[] getBlocks() {
        return blocks;
    }

    public byte[] getData() {
        return data;
    }

    public short getWidth() {
        return width;
    }

    public short getLength() {
        return length;
    }

    public short getHeight() {
        return height;
    }

    public void assemble(String direction, Location loc, boolean destroy) {
        int rotation = 0;
        if (direction.equalsIgnoreCase("E"))
            rotation = 90;
        else if (direction.equalsIgnoreCase("S"))
            rotation = 180;
        else if (direction.equalsIgnoreCase("W"))
            rotation = 270;


        byte[] _blocks = this.blocks.clone();
        byte[] _data = this.data.clone();

        for (int x = 0; x < this.width; ++x) {
            for (int y = 0; y < this.height; ++y) {
                for (int z = 0; z < this.length; ++z) {
                    int index = y * this.width * this.length + z * this.width + x;

                    try {
                        int id = _blocks[index];
                        byte data = _data[index];

                        if (id < 0)
                            id += 256;

                        Block block = rotateLocation(loc, x, y, z, rotation).getBlock();
                        if (id != 0)
                            if (!destroy && id != 61)
                                block.setTypeIdAndData(id, data, true);
                            else if (destroy)
                                block.setTypeIdAndData(0, (byte) 0, true);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    private Location rotateLocation(Location loc, int x, int y, int z, double rotation) {
        int newX, newZ;

        if (rotation == 0 || rotation == 360 || rotation % 90 != 0)
            return new Location(loc.getWorld(), loc.getBlockX() + x, loc.getBlockY() + y,
                loc.getBlockZ() + z);

        if (rotation == 90) {
            newX = -z;
            newZ = x;
        } else if (rotation == 180) {
            newX = -x;
            newZ = -z;
        } else {
            newX = z;
            newZ = -x;
        }
        return new Location(loc.getWorld(), loc.getBlockX() + newX, loc.getBlockY() + y,
            loc.getZ() + newZ);
    }

}
