package me.zath.skyquarries.objects;
/*
 * MC 
 * Created by zAth
 */

import me.zath.skyquarries.SkyQuarries;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.Random;

public class Laser {

    private int entityId;
    private Object dataWatcher;

    private Location location;

    Laser(Location location) {
        try {
            entityId = new Random().nextInt(Integer.MAX_VALUE);

            dataWatcher = SkyQuarries.getSkyQuarries().getReflection().getConstructor_DataWatcher().newInstance((Object) null);

            byte data = (byte) calcData(0, 0, false); //onFire
            data = (byte) calcData(data, 1, false); //Crouched
            data = (byte) calcData(data, 3, false); //Sprinting
            data = (byte) calcData(data, 4, false); //Eating/Drinking/Block
            data = (byte) calcData(data, 5, true);  //Invisible

            SkyQuarries.getSkyQuarries().getReflection().getMethod_DWA().invoke(dataWatcher, 0, data);
            SkyQuarries.getSkyQuarries().getReflection().getMethod_DWA().invoke(dataWatcher, 6, 20F);

            int type = calcType(0, 2, false); //Is Elderly
            type = calcType(type, 4, false);  //Is retracting spikes

            SkyQuarries.getSkyQuarries().getReflection().getMethod_DWA().invoke(dataWatcher, 16, type);

            this.location = location;
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    private void sendPacket(Object packet){
        for(Player player : Bukkit.getOnlinePlayers()){
            sendPacket(player, packet);
        }
    }

    private void sendPacket(Player player, Object packet){
        try {
            SkyQuarries.getSkyQuarries().getReflection().getMethod_SendPacket().invoke(SkyQuarries.getSkyQuarries().getReflection().getConnection(player), packet);
        } catch (IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }
    }


    private void spawn(Player show){
        try {
            Object packet = SkyQuarries.getSkyQuarries().getReflection().getConstructor_PacketPlayOutSpawnEntityLiving().newInstance();

            set(packet, "a", entityId);
            set(packet, "b", 68);
            set(packet, "c", toFixedPointNumber(location.getX()));
            set(packet, "d", toFixedPointNumber(location.getY()));
            set(packet, "e", toFixedPointNumber(location.getZ()));
            set(packet, "f", (int)toPackedByte(location.getYaw()));
            set(packet, "g", (int)toPackedByte(location.getPitch()));
            set(packet, "h", (int)toPackedByte(location.getPitch()));
            set(packet, "i", (byte) 0);
            set(packet, "j", (byte) 0);
            set(packet, "k", (byte) 0);
            set(packet, "l", dataWatcher);

            if(show == null){
                sendPacket(packet);
            }else{
                sendPacket(show,packet);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    void setTarget(LivingEntity target){
        try {
            spawn(null);

            Object packet = SkyQuarries.getSkyQuarries().getReflection().getConstructor_PacketPlayOutEntityMetadata().newInstance();
            set(packet, "a", entityId);
            SkyQuarries.getSkyQuarries().getReflection().getMethod_DWA().invoke(dataWatcher, 17, SkyQuarries.getSkyQuarries().getReflection().getMethod_GetId().invoke(SkyQuarries.getSkyQuarries().getReflection().getMethod_CraftEntityGetHandle().invoke(target)));
            set(packet, "b", SkyQuarries.getSkyQuarries().getReflection().getMethod_DWB().invoke(dataWatcher));
            sendPacket(packet);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void despawn(Player show) {
        try {
            Object packet = SkyQuarries.getSkyQuarries().getReflection().getConstructor_PacketPlayOutEntityDestroy().newInstance((Object) new int[] {entityId});

            if (show == null) {
                sendPacket(packet);
            } else {
                sendPacket(show, packet);
            }
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }
    }


    private int calcData(int data, int id, boolean flag) {
        if (flag) { return Integer.valueOf(data | 1 << id);
        } else {  return Integer.valueOf(data & ~(1 << id)); }
    }

    private int calcType(int type, int id, boolean flag) {
        if (flag) { return Integer.valueOf(type | id);
        } else {  return Integer.valueOf(type & ~id); }
    }

    private byte toPackedByte(float f) {
        return (byte)((int)(f * 256.0F / 360.0F));
    }

    private int toFixedPointNumber(Double d) {
        return (int)(d * 32D);
    }

    private void set(Object instance, String name, Object value) throws Exception {
        Field field = instance.getClass().getDeclaredField(name);
        field.setAccessible(true);
        field.set(instance, value);
    }

    public Location getLocation() {
        return location;
    }

    @Override
    public String toString() {
        return "Laser{" +
            "entityId=" + entityId +
            ", dataWatcher=" + dataWatcher +
            ", location=" + SkyQuarries.getSkyQuarries().getUtils().serializeLocation(location) +
            '}';
    }
}
