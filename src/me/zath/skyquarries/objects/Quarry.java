package me.zath.skyquarries.objects;
/*
 * MC 
 * Created by zAth
 */

import me.zath.skyquarries.SkyQuarries;
import me.zath.skyquarries.events.QuarryBlockBreakEvent;
import me.zath.skyquarries.events.QuarryDropEvent;
import org.bukkit.Bukkit;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Furnace;
import org.bukkit.block.Hopper;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.ExperienceOrb;
import org.bukkit.inventory.FurnaceInventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.Vector;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

public class Quarry {

    //========== Storable ==========\\
    private String ownerName;
    private Integer holeSize;
    private Location furnaceLocation; // must be in the center of south side, 2 block high
    private String direction; // N, S, E, W
    private Boolean enabled;
    //========= Calculated =========\\
    private Integer quarrySize;
    private Furnace furnace;
    private Location hopperFace;
    private Location quarryMin;
    private Location quarryMax;
    private Location holeMin; // 5 block above bedrock
    private Location holeMax; // 1 block below mid
    private Location holeMid; // 4 block high, 1 above furnace
    //========== Internal ==========\\
    private ArmorStand armorStand;
    private Laser laser;
    private Block block;
    private Double secondsToBreak;
    private Double secondsGone;

    public Quarry(String ownerName, Integer holeSize, String serializedFurnaceLocation, String direction, Boolean enabled) {
        this.ownerName = ownerName;
        this.holeSize = holeSize;
        this.furnaceLocation = SkyQuarries.getSkyQuarries().getUtils().deserializeLocation(serializedFurnaceLocation);
        this.direction = direction;
        this.enabled = enabled;

        this.quarrySize = (int) SkyQuarries.getSkyQuarries().getSchematicController().getSchematicHashMap().get(this.holeSize).getLength();
        this.furnace = (Furnace) furnaceLocation.getBlock().getState();
        ItemStack button = this.enabled ?
            SkyQuarries.getSkyQuarries().getConfiguration().getItems().getItemStack_On()
            : SkyQuarries.getSkyQuarries().getConfiguration().getItems().getItemStack_Off();
        this.furnace.getInventory().setSmelting(button);
        this.furnace.getInventory().setFuel(SkyQuarries.getSkyQuarries().getConfiguration().getItems().getItemStack_Destroy());

        this.holeMid = this.furnaceLocation.clone()
            .add(SkyQuarries.getSkyQuarries().getUtils().getDirection(this.direction).clone()
                .multiply(Math.floor(SkyQuarries.getSkyQuarries().getSchematicController().getSchematicHashMap().get(this.holeSize).getLength() / 2))
                .setY(1));

        this.hopperFace = this.furnaceLocation.clone()
            .add(SkyQuarries.getSkyQuarries().getUtils().getDirection(this.direction).clone()
                .multiply(-1)
                .setY(-2));

        this.quarryMin = this.furnaceLocation.clone()
            .add(SkyQuarries.getSkyQuarries().getUtils().getLeftSide(this.direction).clone()
                .multiply(Math.floor(SkyQuarries.getSkyQuarries().getSchematicController().getSchematicHashMap().get(this.holeSize).getLength() / 2))
                .setY(-2));

        this.quarryMax = this.quarryMin.clone()
            .add(SkyQuarries.getSkyQuarries().getUtils().getDirection(this.direction).clone()
                .multiply(SkyQuarries.getSkyQuarries().getSchematicController().getSchematicHashMap().get(this.holeSize).getLength() - 1))
            .add(SkyQuarries.getSkyQuarries().getUtils().getRighttSide(this.direction).clone()
                .multiply(SkyQuarries.getSkyQuarries().getSchematicController().getSchematicHashMap().get(this.holeSize).getLength() - 1))
            .add(new Vector(0.0, SkyQuarries.getSkyQuarries().getSchematicController().getSchematicHashMap().get(this.holeSize).getHeight() - 1, 0.0));

        this.holeMin = this.getHoleMid().clone().subtract(Math.floor(this.holeSize / 2), this.getHoleMid().getBlockY() - 5, Math.floor(this.holeSize / 2));
        this.holeMax = this.getHoleMid().clone().subtract(0, 1, 0).add(Math.floor(this.holeSize / 2), 0, Math.floor(this.holeSize / 2));

        this.block = null;
        this.armorStand = null;
        this.laser = new Laser(SkyQuarries.getSkyQuarries().getUtils().getCenter(this.holeMid, this.direction));
        this.secondsToBreak = 0.0;
        this.secondsGone = 0.0;
    }

    void tick() {
        if (!this.enabled)
            return;

        if(!furnaceLocation.getChunk().isLoaded())
            return;

        if (block == null){
            calcNextBlock();
            return;
        }

        if (this.secondsGone < this.secondsToBreak) {
            this.secondsGone += 0.05; // 0.05 seconds = 1 tick ( delay of the runnable )
            sendBlockBreak((int) Math.max(0, Math.min(9, this.secondsGone * 9.0 / this.secondsToBreak)));// 0-9, other value to remove the animation
            return;
        }

        QuarryBlockBreakEvent quarryBlockBreakEvent = new QuarryBlockBreakEvent(this);
        Bukkit.getPluginManager().callEvent(quarryBlockBreakEvent);

        if (!quarryBlockBreakEvent.isCancelled()) {
            List<ItemStack> realDrops = new ArrayList<>();

            boolean giveExp = false;
            if (getTool() != null && getTool().hasItemMeta() && getTool().getItemMeta().hasEnchants()) {
                if (getTool().getItemMeta().hasEnchant(Enchantment.SILK_TOUCH)) {
                    realDrops.add(new ItemStack(block.getType(), 1));
                } else if (getTool().getItemMeta().hasEnchant(Enchantment.LOOT_BONUS_BLOCKS)) {
                    giveExp = true;
                    realDrops.addAll(SkyQuarries.getSkyQuarries().getUtils().getFortuneDrops(getTool().getItemMeta().getEnchantLevel(Enchantment.LOOT_BONUS_BLOCKS), block.getDrops(getTool()).stream().collect(Collectors.toList())));
                } else {
                    realDrops.addAll(block.getDrops(getTool()));
                    giveExp = true;
                }
            } else {
                realDrops.addAll(block.getDrops(getTool()));
                giveExp = true;
            }

            if (giveExp) {
                int exp = 0;
                if (block.getType().name().contains("ORE")) {
                    if (block.getType() == Material.COAL_ORE)
                        exp = SkyQuarries.getSkyQuarries().getUtils().getRandomNumber(0, 2);

                    if (block.getType() == Material.DIAMOND_ORE || block.getType() == Material.EMERALD_ORE)
                        exp = SkyQuarries.getSkyQuarries().getUtils().getRandomNumber(3, 7);

                    if (block.getType() == Material.QUARTZ_ORE || block.getType() == Material.LAPIS_ORE)
                        exp = SkyQuarries.getSkyQuarries().getUtils().getRandomNumber(2, 5);

                    if (block.getType() == Material.REDSTONE_ORE)
                        exp = SkyQuarries.getSkyQuarries().getUtils().getRandomNumber(1, 5);
                }

                if (exp != 0) {
                    ((ExperienceOrb) hopperFace.getWorld().spawnEntity(hopperFace, EntityType.EXPERIENCE_ORB)).setExperience(exp);
                }
            }

            QuarryDropEvent quarryDropEvent = new QuarryDropEvent(this, realDrops);
            Bukkit.getServer().getPluginManager().callEvent(quarryDropEvent);

            ItemStack[] dropsArray = new ItemStack[quarryDropEvent.getDrops().size()];
            dropsArray = quarryDropEvent.getDrops().toArray(dropsArray);

            Hopper hopper = (Hopper) furnace.getLocation().subtract(0.0, 2, 0.0).getBlock().getState();
            HashMap<Integer, ItemStack> failedToAdd = hopper.getInventory().addItem(dropsArray);

            if (!failedToAdd.isEmpty()) {
                failedToAdd.values().forEach(itemStack -> hopperFace.getWorld().dropItemNaturally(hopperFace, itemStack));
            }

            sendBlockBreak(-1); // remove animation
            block.getWorld().playEffect(block.getLocation(), Effect.STEP_SOUND, block.getTypeId());
            block.getWorld().playEffect(block.getLocation(), Effect.TILE_BREAK, block.getTypeId());
            block.setType(Material.AIR);

            if (getTool() != null) {
                if (getTool().hasItemMeta() && getTool().getItemMeta().hasEnchants()
                    && getTool().getItemMeta().hasEnchant(Enchantment.DURABILITY)
                    && SkyQuarries.getSkyQuarries().getUtils().getRandomNumber(0, 100) > (100 / (getTool().getItemMeta().getEnchantLevel(Enchantment.DURABILITY) + 1)))
                    return;

                if (getTool().getDurability() >= getTool().getType().getMaxDurability())
                    setTool(null);
                else
                    getTool().setDurability((short) (getTool().getDurability() + 1));

            }
        }

        calcNextBlock();
        ItemStack button = this.enabled
            ? SkyQuarries.getSkyQuarries().getConfiguration().getItems().getItemStack_On()
            : SkyQuarries.getSkyQuarries().getConfiguration().getItems().getItemStack_Off();
        // i dont know why smelting is disappearing every time a block is broken, so im setting it frequently
        this.furnace.getInventory().setSmelting(button);
    }

    private void calcNextBlock() {
        new AsyncCalcule(this.holeMin, this.holeMax, new AsyncCalcule.Callback<Block>() {
            @Override
            public void onSuccess(Block block) {
                    setBlock(block);
                    setSecondsGone(0.0);
                    setSecondsToBreak(SkyQuarries.getSkyQuarries().getUtils().getSecondsToBreak(getTool(), getBlock()));

                    setArmorStand(SkyQuarries.getSkyQuarries().getUtils().getCenter(block.getLocation(), getDirection()));
                    getLaser().despawn(null);
                    setLaser(getLaser().getLocation());
            }

            @Override
            public void onFailure(String cause) {
                if (cause.equals("Valid block not found")) {
                    setBlock(null);
                    setSecondsGone(0.0);
                    setSecondsToBreak(Double.MAX_VALUE);

                    if(getArmorStand() != null)
                        getArmorStand().remove();
                    getLaser().despawn(null);
                } else if(cause.equals("toStop block found")){
                    setEnabled(!isEnabled());
                    setBlock(null);
                    setSecondsGone(0.0);
                    sendBlockBreak(-1);

                    if(getArmorStand() != null)
                        getArmorStand().remove();
                    getLaser().despawn(null);

                    FurnaceInventory furnaceInventory = getFurnace().getInventory();
                    ItemStack button = isEnabled()
                        ? SkyQuarries.getSkyQuarries().getConfiguration().getItems().getItemStack_On()
                        : SkyQuarries.getSkyQuarries().getConfiguration().getItems().getItemStack_Off();
                    furnaceInventory.setSmelting(button);
                }
            }
        });
    }

    public void sendBlockBreak(int damage) { // block break animation
        if (block == null)
            return;

        try {
            Object worldServer = SkyQuarries.getSkyQuarries().getReflection().getMethod_GetHandle().invoke(block.getWorld());
            int dimension = (int) worldServer.getClass().getField("dimension").get(worldServer);
            Object blockPosition = SkyQuarries.getSkyQuarries().getReflection().getConstructor_BlockPosition().newInstance(block.getX(), block.getY(), block.getZ());
            Object packet = SkyQuarries.getSkyQuarries().getReflection().getConstructor_PacketPlayOutBlockBreakAnimation().newInstance(dimension, blockPosition, damage);

            Object dedicatedServerList = SkyQuarries.getSkyQuarries().getReflection().getMethod_CraftServerGetHandle().invoke(SkyQuarries.getSkyQuarries().getServer());
            SkyQuarries.getSkyQuarries().getReflection().getMethod_SendPacketNearby().invoke(dedicatedServerList, block.getX(), block.getY(), block.getZ(), 50 /* radius */, dimension, packet);
        } catch (InstantiationException | IllegalAccessException | NoSuchFieldException | InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    //========== Getters ==========\\
    public Location getHopperFace() {
        return hopperFace;
    }

    public ArmorStand getArmorStand() {
        return armorStand;
    }

    public Laser getLaser() {
        return laser;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public Integer getHoleSize() {
        return holeSize;
    }

    public Location getFurnaceLocation() {
        return furnaceLocation;
    }

    public String getDirection() {
        return direction;
    }

    public Boolean isEnabled() {
        return enabled;
    }

    public Integer getQuarrySize() {
        return quarrySize;
    }

    public Furnace getFurnace() {
        return furnace;
    }

    public ItemStack getTool() {
        return this.furnace.getInventory().getResult();
    }

    public Location getQuarryMin() {
        return quarryMin;
    }

    public Location getQuarryMax() {
        return quarryMax;
    }

    public Location getHoleMin() {
        return holeMin;
    }

    public Location getHoleMax() {
        return holeMax;
    }

    public Location getHoleMid() {
        return holeMid;
    }

    public Block getBlock() {
        return block;
    }

    public Double getSecondsToBreak() {
        return secondsToBreak;
    }

    public Double getSecondsGone() {
        return secondsGone;
    }

    //========== Setters ==========\\
    public void setHopperFace(Location hopperFace) {
        this.hopperFace = hopperFace;
    }

    public void setArmorStand(Location location) {
        if(this.armorStand != null)
            this.armorStand.remove();

        ArmorStand armorStand = location.getWorld().spawn(location, ArmorStand.class);
        armorStand.setGravity(false);
        armorStand.setVisible(false);

        this.armorStand = armorStand;
    }

    public void setLaser(Location location) {
        Laser laser = new Laser(location);
        laser.setTarget(this.armorStand);

        this.laser = laser;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public void setTool(ItemStack tool) {
        this.furnace.getInventory().setResult(tool);
    }

    public void setBlock(Block block) {
        this.block = block;
    }

    public void setSecondsToBreak(Double secondsToBreak) {
        this.secondsToBreak = secondsToBreak;
    }

    public void setSecondsGone(Double secondsGone) {
        this.secondsGone = secondsGone;
    }

    //========== others ==========\\
    @Override
    public String toString() {
        return "Quarry{" +
            "ownerName='" + ownerName + '\'' +
            ", holeSize=" + holeSize +
            ", furnaceLocation=" + SkyQuarries.getSkyQuarries().getUtils().serializeLocation(this.furnaceLocation) +
            ", direction='" + direction + '\'' +
            ", enabled=" + enabled +
            ", quarrySize=" + quarrySize +
            ", hopperFace=" + SkyQuarries.getSkyQuarries().getUtils().serializeLocation(this.hopperFace) +
            ", quarryMin=" + SkyQuarries.getSkyQuarries().getUtils().serializeLocation(this.quarryMin) +
            ", quarryMax=" + SkyQuarries.getSkyQuarries().getUtils().serializeLocation(this.quarryMax) +
            ", holeMin=" + SkyQuarries.getSkyQuarries().getUtils().serializeLocation(this.holeMin) +
            ", holeMax=" + SkyQuarries.getSkyQuarries().getUtils().serializeLocation(this.holeMax) +
            ", holeMid=" + SkyQuarries.getSkyQuarries().getUtils().serializeLocation(this.holeMid) +
            ", armorStand=" + armorStand.toString() +
            ", laser=" + laser.toString() +
            ", block=" + block.toString() +
            ", secondsToBreak=" + secondsToBreak +
            ", secondsGone=" + secondsGone +
            '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Quarry quarry = (Quarry) o;

        if (!ownerName.equals(quarry.ownerName)) return false;
        if (!holeSize.equals(quarry.holeSize)) return false;
        if (!furnaceLocation.equals(quarry.furnaceLocation)) return false;
        if (!direction.equals(quarry.direction)) return false;
        if (!enabled.equals(quarry.enabled)) return false;
        if (quarrySize != null ? !quarrySize.equals(quarry.quarrySize) : quarry.quarrySize != null) return false;
        if (furnace != null ? !furnace.equals(quarry.furnace) : quarry.furnace != null) return false;
        if (hopperFace != null ? !hopperFace.equals(quarry.hopperFace) : quarry.hopperFace != null) return false;
        if (quarryMin != null ? !quarryMin.equals(quarry.quarryMin) : quarry.quarryMin != null) return false;
        if (quarryMax != null ? !quarryMax.equals(quarry.quarryMax) : quarry.quarryMax != null) return false;
        if (holeMin != null ? !holeMin.equals(quarry.holeMin) : quarry.holeMin != null) return false;
        if (holeMax != null ? !holeMax.equals(quarry.holeMax) : quarry.holeMax != null) return false;
        return holeMid != null ? holeMid.equals(quarry.holeMid) : quarry.holeMid == null;

    }

    @Override
    public int hashCode() {
        int result = ownerName.hashCode();
        result = 31 * result + holeSize.hashCode();
        result = 31 * result + furnaceLocation.hashCode();
        result = 31 * result + direction.hashCode();
        result = 31 * result + enabled.hashCode();
        result = 31 * result + (quarrySize != null ? quarrySize.hashCode() : 0);
        result = 31 * result + (furnace != null ? furnace.hashCode() : 0);
        result = 31 * result + (hopperFace != null ? hopperFace.hashCode() : 0);
        result = 31 * result + (quarryMin != null ? quarryMin.hashCode() : 0);
        result = 31 * result + (quarryMax != null ? quarryMax.hashCode() : 0);
        result = 31 * result + (holeMin != null ? holeMin.hashCode() : 0);
        result = 31 * result + (holeMax != null ? holeMax.hashCode() : 0);
        result = 31 * result + (holeMid != null ? holeMid.hashCode() : 0);
        return result;
    }
}
