package me.zath.skyquarries.objects;
/*
 * MC 
 * Created by zAth
 */

public class IdDataPair {

    private Integer id;
    private Integer data;
    private boolean toStop;

    public IdDataPair(Integer id, Integer data, boolean toStop) {
        this.id = id;
        this.data = data;
        this.toStop = toStop;
    }

    public Integer getId() {
        return id;
    }

    public Integer getData() {
        return data;
    }

    public boolean isToStop() {
        return toStop;
    }

    @Override
    public String toString() {
        return "IdDataPair{" +
            "id=" + id +
            ", data=" + data +
            ", toStop=" + toStop +
            '}';
    }
}
