package me.zath.skyquarries.objects;
/*
 * MC 
 * Created by zAth
 */

import me.zath.skyquarries.SkyQuarries;
import org.bukkit.scheduler.BukkitRunnable;

public class QuarryRunnable extends BukkitRunnable {

    public QuarryRunnable(SkyQuarries skyQuarries) {
        this.runTaskTimer(skyQuarries, 0, 1);
    }

    @Override
    public void run() {
        SkyQuarries.getSkyQuarries().getQuarryController().getQuarryHashMap().values().forEach(Quarry::tick);
    }

    public void stop(){
        this.cancel();
    }

    public void start(){
        SkyQuarries.getSkyQuarries().setQuarryRunnable(new QuarryRunnable(SkyQuarries.getSkyQuarries()));
    }

}
