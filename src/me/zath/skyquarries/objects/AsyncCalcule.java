package me.zath.skyquarries.objects;
/*
 * MC 
 * Created by zAth
 */

import me.zath.skyquarries.SkyQuarries;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.scheduler.BukkitRunnable;

public class AsyncCalcule {

    AsyncCalcule(Location min, Location max, final Callback<Block> callback) {
        new BukkitRunnable() {
            @Override
            public void run() {
                for (int y = max.getBlockY(); y >= min.getBlockY(); y--) {
                    for (int x = max.getBlockX(); x >= min.getBlockX(); x--) {
                        for (int z = max.getBlockZ(); z >= min.getBlockZ(); z--) {
                            final Block block = new Location(min.getWorld(), x, y, z).getBlock();

                            if(SkyQuarries.getSkyQuarries().getBlacklistController().isInBlacklist(block.getTypeId(), block.getData())
                                && SkyQuarries.getSkyQuarries().getBlacklistController().isStopper(block.getTypeId(), block.getData())){
                                new BukkitRunnable() {
                                    @Override
                                    public void run() {
                                        callback.onFailure("toStop block found");
                                    }
                                }.runTask(SkyQuarries.getSkyQuarries());

                                return;
                            }

                            if (!SkyQuarries.getSkyQuarries().getBlacklistController().isInBlacklist(block.getTypeId(), block.getData())) {
                                new BukkitRunnable() {
                                    @Override
                                    public void run() {
                                        callback.onSuccess(block);
                                    }
                                }.runTask(SkyQuarries.getSkyQuarries());

                                return;
                            }
                        }
                    }
                }

                new BukkitRunnable() {
                    @Override
                    public void run() {
                        callback.onFailure("Valid block not found");
                    }
                }.runTask(SkyQuarries.getSkyQuarries());
            }
        }.runTaskAsynchronously(SkyQuarries.getSkyQuarries());
    }

    public interface Callback<T> {
        void onSuccess(T result);

        void onFailure(String cause);
    }

}
