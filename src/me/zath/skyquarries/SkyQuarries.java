package me.zath.skyquarries;
/*
 * MC
 * Created by zAth
 */

import me.zath.skyquarries.commands.Command;
import me.zath.skyquarries.controllers.*;
import me.zath.skyquarries.listeners.Listener;
import me.zath.skyquarries.objects.QuarryRunnable;
import me.zath.skyquarries.utils.Reflection;
import me.zath.skyquarries.utils.SQL;
import me.zath.skyquarries.utils.Utils;
import org.bukkit.Bukkit;
import org.bukkit.event.HandlerList;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;

public class SkyQuarries extends JavaPlugin {

    // change some blocks data when rotated, like stairs, furnace, etc

    // make it need fuel to work

    private static SkyQuarries skyQuarries;
    private Utils utils;
    private Reflection reflection;
    private Config config;
    private SupportController supportController;
    private SchematicController schematicController;
    private ToolBreakBlocksController toolBreakBlocksController;
    private BlacklistController blacklistController;
    private QuarryController quarryController;
    private SQL sql;
    private QuarryRunnable quarryRunnable;

    public static SkyQuarries getSkyQuarries() {
        return skyQuarries;
    }

    @Override
    public void onEnable() {
        File file = new File(getDataFolder(), "config.yml");
        if (!(file.exists())) {
            try {
                saveResource("config.yml", false);
            } catch (Exception ignored) {
            }
        }
        saveDefaultConfig();
        File db = new File(getDataFolder(), "skyquarries.db");
        if (!(db.exists())) {
            try {
                saveResource("skyquarries.db", false);
            } catch (Exception ignored) {
            }
        }

        skyQuarries = this;
        utils = new Utils();
        reflection = new Reflection();
        config = new Config();
        supportController = new SupportController(config);
        schematicController = new SchematicController();
        toolBreakBlocksController = new ToolBreakBlocksController();
        blacklistController = new BlacklistController();
        quarryController = new QuarryController();
        sql = new SQL(db);
        registerCommands();
        registerEvents();
        quarryRunnable = new QuarryRunnable(this);
        Bukkit.getConsoleSender().sendMessage("§6<§8-----------------------------§6>");
        Bukkit.getConsoleSender().sendMessage("§6" + getDescription().getName() + " §8v.§6" + getDescription().getVersion() + " §8de§6 "
            + getDescription().getAuthors() + " §2Ativado");
        Bukkit.getConsoleSender().sendMessage("§6<§8-----------------------------§6>");
    }

    @Override
    public void onDisable() {
        quarryController.getQuarryHashMap().values().forEach(quarry -> {
            quarry.sendBlockBreak(-1);
            quarry.getLaser().despawn(null);
            sql.updateQuarry(quarry);
        });
        sql.closeConnection();
        Bukkit.getConsoleSender().sendMessage("§6<§8-----------------------------§6>");
        Bukkit.getConsoleSender().sendMessage("§6" + getDescription().getName() + " §8v.§6" + getDescription().getVersion() + " §8de§6 "
            + getDescription().getAuthors() + " §4Desativado");
        Bukkit.getConsoleSender().sendMessage("§6<§8-----------------------------§6>");
        HandlerList.unregisterAll();
    }

    private void registerCommands() {
        getServer().getPluginCommand("skyquarries").setExecutor(new Command());
    }

    private void registerEvents() {
        getServer().getPluginManager().registerEvents(new Listener(), this);
    }

    public Utils getUtils() {
        return utils;
    }

    public Reflection getReflection() {
        return reflection;
    }

    public Config getConfiguration() {
        return config;
    }

    public SupportController getSupportController() {
        return supportController;
    }

    public void setSupportController(SupportController supportController) {
        this.supportController = supportController;
    }

    public SchematicController getSchematicController() {
        return schematicController;
    }

    public ToolBreakBlocksController getToolBreakBlocksController() {
        return toolBreakBlocksController;
    }

    public BlacklistController getBlacklistController() {
        return blacklistController;
    }

    public QuarryController getQuarryController() {
        return quarryController;
    }

    public SQL getSql() {
        return sql;
    }

    public QuarryRunnable getQuarryRunnable() {
        return quarryRunnable;
    }

    public void setQuarryRunnable(QuarryRunnable quarryRunnable) {
        this.quarryRunnable = quarryRunnable;
    }

    public ItemStack getQuarry(int holeSize) throws Exception {
        if (!schematicController.getSchematicHashMap().containsKey(holeSize)) {

            throw new Exception("There is no quarry with that hole size");
        }

        return config.getItems().getItemStack_Quarry(holeSize);
    }
}
