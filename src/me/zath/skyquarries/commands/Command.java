package me.zath.skyquarries.commands;
/*
 * MC 
 * Created by zAth
 */

import me.zath.skyquarries.SkyQuarries;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Command implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, org.bukkit.command.Command cmd, String s, String[] args) {
        if (cmd.getName().equalsIgnoreCase("skyquarries")) {
            if (sender instanceof Player) {
                Player player = (Player) sender;
                if (!player.hasPermission("skyquarries.command")) {
                    player.sendMessage(SkyQuarries.getSkyQuarries().getConfiguration().getMsg_NoPermission());
                    return true;
                }
            }

            if (args.length == 1) {
                if (args[0].equalsIgnoreCase("off")) {
                    SkyQuarries.getSkyQuarries().getQuarryRunnable().stop();
                    sender.sendMessage(SkyQuarries.getSkyQuarries().getConfiguration().getMsg_QuarriesDisabled());
                } else if (args[0].equalsIgnoreCase("on")) {
                    SkyQuarries.getSkyQuarries().getQuarryRunnable().start();
                    sender.sendMessage(SkyQuarries.getSkyQuarries().getConfiguration().getMsg_QuarriesEnabled());
                } else {
                    sender.sendMessage(SkyQuarries.getSkyQuarries().getConfiguration().getMsg_CommandOnOffUsage());
                }
            } else if (args.length == 3) {
                if (args[0].equalsIgnoreCase("give") && SkyQuarries.getSkyQuarries().getUtils().isInt(args[2])) {
                    Player player = SkyQuarries.getSkyQuarries().getServer().getPlayer(args[1]);
                    if (player == null) {
                        sender.sendMessage(SkyQuarries.getSkyQuarries().getConfiguration().getMsg_PlayerOffline());
                        return true;
                    }

                    int holeSize = Integer.parseInt(args[2]);
                    if (!SkyQuarries.getSkyQuarries().getSchematicController().getSchematicHashMap().containsKey(holeSize)) {
                        sender.sendMessage(SkyQuarries.getSkyQuarries().getConfiguration().getMsg_SizeDoesntExist());
                        return true;
                    }

                    player.getInventory().addItem(SkyQuarries.getSkyQuarries().getConfiguration().getItems().getItemStack_Quarry(holeSize));
                    player.sendMessage(SkyQuarries.getSkyQuarries().getConfiguration().getMsg_QuarryReceived().replaceAll("%holeSize%", "" + holeSize));
                    sender.sendMessage(SkyQuarries.getSkyQuarries().getConfiguration().getMsg_QuarryGiven().replaceAll("%playerName%", player.getName()).replaceAll("%holeSize%", "" + holeSize));
                } else {
                    sender.sendMessage(SkyQuarries.getSkyQuarries().getConfiguration().getMsg_CommandGiveUsage());
                }
            } else {
                sender.sendMessage(SkyQuarries.getSkyQuarries().getConfiguration().getMsg_CommandOnOffUsage());
                sender.sendMessage(SkyQuarries.getSkyQuarries().getConfiguration().getMsg_CommandGiveUsage());
            }
        }
        return false;
    }

}
