package me.zath.skyquarries.support;
/*
 * MC 
 * Created by zAth
 */

import me.zath.skyblock.SkyBlock;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;

public class SkyBlockSupport implements ISupportable {

    @Override
    public boolean canBuild(Player player, Block block) {
        return SkyBlock.getCore().canPlace(player.getName(), block.getLocation());
    }

}
