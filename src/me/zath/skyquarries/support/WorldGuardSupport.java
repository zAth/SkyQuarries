package me.zath.skyquarries.support;
/*
 * MC 
 * Created by zAth
 */

import com.sk89q.worldguard.bukkit.WorldGuardPlugin;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;

public class WorldGuardSupport implements ISupportable {

    @Override
    public boolean canBuild(Player player, Block block) {
        return WorldGuardPlugin.inst().canBuild(player, block);
    }

}
