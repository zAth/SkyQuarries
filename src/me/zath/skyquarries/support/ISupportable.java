package me.zath.skyquarries.support;
/*
 * MC 
 * Created by zAth
 */

import org.bukkit.block.Block;
import org.bukkit.entity.Player;

public interface ISupportable {

    boolean canBuild(Player player, Block block);

}
