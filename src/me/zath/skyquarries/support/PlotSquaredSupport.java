package me.zath.skyquarries.support;
/*
 * MC 
 * Created by zAth
 */

import com.intellectualcrafters.plot.api.PlotAPI;
import com.intellectualcrafters.plot.object.Plot;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;

public class PlotSquaredSupport implements ISupportable {

    @Override
    public boolean canBuild(Player player, Block block) {
        PlotAPI plotAPI = new PlotAPI();
        Plot plot = plotAPI.getPlot(block.getLocation());
        return plot != null && plot.isAdded(player.getUniqueId());
    }

}
