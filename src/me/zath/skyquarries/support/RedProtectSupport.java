package me.zath.skyquarries.support;
/*
 * MC 
 * Created by zAth
 */

import br.net.fabiozumbi12.RedProtect.Bukkit.RedProtect;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;

public class RedProtectSupport implements ISupportable {

    @Override
    public boolean canBuild(Player player, Block block) {
        return RedProtect.get().getAPI().getRegion(block.getLocation()).canBuild(player);
    }

}
