package me.zath.skyquarries.events;
/*
 * MC 
 * Created by zAth
 */

import me.zath.skyquarries.objects.Quarry;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class QuarryBlockBreakEvent extends Event {

    private HandlerList handlers = new HandlerList();
    private Quarry quarry;
    private Boolean cancelled;

    public QuarryBlockBreakEvent(Quarry quarry) {
        this.quarry = quarry;
        this.cancelled = false;
    }

    public Quarry getQuarry() {
        return quarry;
    }

    public Boolean isCancelled() {
        return cancelled;
    }

    public void setCancelled(Boolean cancelled) {
        this.cancelled = cancelled;
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }
}
