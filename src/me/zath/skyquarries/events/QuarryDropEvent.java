package me.zath.skyquarries.events;
/*
 * MC 
 * Created by zAth
 */

import me.zath.skyquarries.objects.Quarry;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.bukkit.inventory.ItemStack;

import java.util.List;

public class QuarryDropEvent extends Event {

    private HandlerList handlers = new HandlerList();
    private Quarry quarry;
    private List<ItemStack> drops;

    public QuarryDropEvent(Quarry quarry, List<ItemStack> drops) {
        this.quarry = quarry;
        this.drops = drops;
    }

    public Quarry getQuarry() {
        return quarry;
    }

    public List<ItemStack> getDrops() {
        return drops;
    }

    public void setDrops(List<ItemStack> drops) {
        this.drops = drops;
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }
}
