package me.zath.skyquarries;
/*
 * MC 
 * Created by zAth
 */

import me.zath.skyquarries.utils.ItemBuilder;
import me.zath.skyquarries.utils.Items;

import java.util.List;
import java.util.stream.Collectors;

public class Config {

    private String msg_NotOwner, msg_QuarryNear, msg_NoPermission, msg_QuarriesEnabled, msg_QuarriesDisabled, msg_PlayerOffline
        , msg_SizeDoesntExist, msg_QuarryReceived, msg_QuarryGiven, msg_CommandOnOffUsage, msg_CommandGiveUsage, msg_InCooldown;
    private Integer cooldown;
    private Boolean sql_ShowPossibleSqlErrors;
    private List<String> quarry_WorldWhitelist, blackList;

    private Items items;
    
    Config(){
        items = new Items();

        blackList = getList("BlackList");
        quarry_WorldWhitelist = getList("WorldWhitelist");
        sql_ShowPossibleSqlErrors = getBoolean("Sql.ShowPossibleSqlErrors");
        cooldown = getInt("Cooldown");
        msg_InCooldown = getString("Messages.InCooldown").replaceAll("&", "§");
        msg_NotOwner = getString("Messages.NotOwner").replaceAll("&", "§");
        msg_QuarryNear = getString("Messages.QuarryNear").replaceAll("&", "§");
        msg_NoPermission = getString("Messages.NoPermission").replaceAll("&", "§");
        msg_QuarriesEnabled = getString("Messages.QuarriesEnabled").replaceAll("&", "§");
        msg_QuarriesDisabled = getString("Messages.QuarriesDisabled").replaceAll("&", "§");
        msg_PlayerOffline = getString("Messages.PlayerOffline").replaceAll("&", "§");
        msg_SizeDoesntExist = getString("Messages.SizeDoesntExist").replaceAll("&", "§");
        msg_QuarryReceived = getString("Messages.QuarryReceived").replaceAll("&", "§");
        msg_QuarryGiven = getString("Messages.QuarryGiven").replaceAll("&", "§");
        msg_CommandOnOffUsage = getString("Messages.CommandOnOffUsage").replaceAll("&", "§");
        msg_CommandGiveUsage = getString("Messages.CommandGiveUsage").replaceAll("&", "§");
        items.setItemStack_Destroy(
            new ItemBuilder(getInt("Items.Destroy.Id"), getInt("Items.Destroy.Data"))
                .withName(getString("Items.Destroy.Name").replaceAll("&", "§"))
                .withLore(makeListColory(getList("Items.Destroy.Lore")))
                .withGlow(getBoolean("Items.Destroy.Glow"))
            .build());
        items.setItemStack_On(
            new ItemBuilder(getInt("Items.Enabled.Id"), getInt("Items.Enabled.Data"))
                .withName(getString("Items.Enabled.Name").replaceAll("&", "§"))
                .withLore(makeListColory(getList("Items.Enabled.Lore")))
                .withGlow(getBoolean("Items.Enabled.Glow"))
            .build());
        items.setItemStack_Off(
            new ItemBuilder(getInt("Items.Disabled.Id"), getInt("Items.Disabled.Data"))
                .withName(getString("Items.Disabled.Name").replaceAll("&", "§"))
                .withLore(makeListColory(getList("Items.Disabled.Lore")))
                .withGlow(getBoolean("Items.Disabled.Glow"))
            .build());
        items.setItemStack_Quarry(
            new ItemBuilder(61, 0)
                .withName(getString("Items.Quarry.Name").replaceAll("&", "§"))
                .withLore(makeListColory(getList("Items.Quarry.Lore")))
                .withGlow(getBoolean("Items.Quarry.Glow"))
                .build());
    }

    public Items getItems() {
        return items;
    }

    public List<String> getBlackList() {
        return blackList;
    }

    public List<String> getQuarry_WorldWhitelist() {
        return quarry_WorldWhitelist;
    }

    public String getMsg_InCooldown() {
        return msg_InCooldown;
    }

    public String getMsg_NoPermission() {
        return msg_NoPermission;
    }

    public String getMsg_QuarriesEnabled() {
        return msg_QuarriesEnabled;
    }

    public String getMsg_QuarriesDisabled() {
        return msg_QuarriesDisabled;
    }

    public String getMsg_PlayerOffline() {
        return msg_PlayerOffline;
    }

    public String getMsg_SizeDoesntExist() {
        return msg_SizeDoesntExist;
    }

    public String getMsg_QuarryReceived() {
        return msg_QuarryReceived;
    }

    public String getMsg_QuarryGiven() {
        return msg_QuarryGiven;
    }

    public String getMsg_CommandOnOffUsage() {
        return msg_CommandOnOffUsage;
    }

    public String getMsg_CommandGiveUsage() {
        return msg_CommandGiveUsage;
    }

    public String getMsg_QuarryNear() {
        return msg_QuarryNear;
    }

    public Boolean getBoolean(String dir) {
        Boolean toReturn;
        try {
            toReturn = SkyQuarries.getSkyQuarries().getConfig().getBoolean(dir);
        } catch (Exception e) {
            SkyQuarries.getSkyQuarries().getServer().getPluginManager().disablePlugin(SkyQuarries.getSkyQuarries());
            throw new IllegalArgumentException("Invalid config: " + e.getMessage() + "\n");
        }
        return toReturn;
    }

    private String getString(String dir) {
        String toReturn;
        try {
            toReturn = SkyQuarries.getSkyQuarries().getConfig().getString(dir);
        } catch (Exception e) {
            SkyQuarries.getSkyQuarries().getServer().getPluginManager().disablePlugin(SkyQuarries.getSkyQuarries());
            throw new IllegalArgumentException("Invalid config: " + e.getMessage() + "\n");
        }
        return toReturn;
    }

    private int getInt(String dir) {
        int toReturn;
        try {
            toReturn = SkyQuarries.getSkyQuarries().getConfig().getInt(dir);
        } catch (Exception e) {
            SkyQuarries.getSkyQuarries().getServer().getPluginManager().disablePlugin(SkyQuarries.getSkyQuarries());
            throw new IllegalArgumentException("Invalid config: " + e.getMessage() + "\n");
        }
        return toReturn;
    }

    private List<String> getList(String dir) {
        List<String> toReturn;
        try {
            toReturn = SkyQuarries.getSkyQuarries().getConfig().getStringList(dir);
        } catch (Exception e) {
            SkyQuarries.getSkyQuarries().getServer().getPluginManager().disablePlugin(SkyQuarries.getSkyQuarries());
            throw new IllegalArgumentException("Invalid config: " + e.getMessage() + "\n");
        }
        return toReturn;
    }

    private List<String> makeListColory(List<String> l) {
        return l.stream().map(str -> str.replaceAll("&", "§")).collect(Collectors.toList());
    }

    public String getMsg_NotOwner() {
        return msg_NotOwner;
    }

    public int getCooldown() {
        return cooldown;
    }

    public boolean getSql_ShowPossibleSqlErrors() {
        return sql_ShowPossibleSqlErrors;
    }
}
