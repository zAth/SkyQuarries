package me.zath.skyquarries.controllers;
/*
 * MC 
 * Created by zAth
 */

import me.zath.skyquarries.objects.IdDataPair;
import org.bukkit.block.Block;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

//i know, there's not all the id's and data's in here
public class ToolBreakBlocksController {

    private static HashMap<String, List<IdDataPair>> toolBreakBlocks;

    public ToolBreakBlocksController() {
        toolBreakBlocks = new HashMap<>();

        //========== "AXE" ==========\\
        create("AXE", 127);
        create("AXE", 91);
        create("AXE", 86);
        create("AXE", 106);
        create("AXE", 103);
        create("AXE", 176);
        create("AXE", 177);
        create("AXE", 425);
        create("AXE", 47);
        create("AXE", 54);
        create("AXE", 58);
        create("AXE", 151);
        create("AXE", 178);
        create("AXE", 85);
        create("AXE", 107);
        create("AXE", 183);
        create("AXE", 184);
        create("AXE", 185);
        create("AXE", 186);
        create("AXE", 187);
        create("AXE", 188);
        create("AXE", 189);
        create("AXE", 190);
        create("AXE", 191);
        create("AXE", 192);
        create("AXE", 99);
        create("AXE", 100);
        create("AXE", 84);
        create("AXE", 65);
        create("AXE", 25);
        create("AXE", 63);
        create("AXE", 68);
        create("AXE", 323);
        create("AXE", 146);
        create("AXE", 5);
        create("AXE", 17);
        create("AXE", 43, 2);
        create("AXE", 44, 2);
        create("AXE", 53);
        create("AXE", 64);
        create("AXE", 72);
        create("AXE", 96);
        create("AXE", 125);
        create("AXE", 126);
        create("AXE", 134);
        create("AXE", 135);
        create("AXE", 136);
        create("AXE", 143);
        create("AXE", 162);
        create("AXE", 163);
        create("AXE", 164);
        create("AXE", 324);
        //========== "PICKAXE" ==========\\
        create("PICKAXE", 79);
        create("PICKAXE", 174);
        create("PICKAXE", 212);
        create("PICKAXE", 145);
        create("PICKAXE", 152);
        create("PICKAXE", 379);
        create("PICKAXE", 118);
        create("PICKAXE", 101);
        create("PICKAXE", 330);
        create("PICKAXE", 167);
        create("PICKAXE", 154);
        create("PICKAXE", 147);
        create("PICKAXE", 148);
        create("PICKAXE", 42);
        create("PICKAXE", 22);
        create("PICKAXE", 57);
        create("PICKAXE", 41);
        create("PICKAXE", 133);
        create("PICKAXE", 219);
        create("PICKAXE", 220);
        create("PICKAXE", 221);
        create("PICKAXE", 222);
        create("PICKAXE", 223);
        create("PICKAXE", 224);
        create("PICKAXE", 225);
        create("PICKAXE", 226);
        create("PICKAXE", 227);
        create("PICKAXE", 228);
        create("PICKAXE", 229);
        create("PICKAXE", 230);
        create("PICKAXE", 231);
        create("PICKAXE", 232);
        create("PICKAXE", 233);
        create("PICKAXE", 234);
        create("PICKAXE", 27);
        create("PICKAXE", 28);
        create("PICKAXE", 66);
        create("PICKAXE", 157);
        create("PICKAXE", 49);
        create("PICKAXE", 56);
        create("PICKAXE", 129);
        create("PICKAXE", 14);
        create("PICKAXE", 15);
        create("PICKAXE", 16);
        create("PICKAXE", 73);
        create("PICKAXE", 74);
        create("PICKAXE", 21);
        create("PICKAXE", 1);
        create("PICKAXE", 173);
        create("PICKAXE", 155);
        create("PICKAXE", 45);
        create("PICKAXE", 4);
        create("PICKAXE", 43, 3);
        create("PICKAXE", 44, 3);
        create("PICKAXE", 48);
        create("PICKAXE", 67);
        create("PICKAXE", 139);
        create("PICKAXE", 251);
        create("PICKAXE", 168);
        create("PICKAXE", 23);
        create("PICKAXE", 158);
        create("PICKAXE", 116);
        create("PICKAXE", 121);
        create("PICKAXE", 130);
        create("PICKAXE", 61);
        create("PICKAXE", 62);
        create("PICKAXE", 235);
        create("PICKAXE", 236);
        create("PICKAXE", 237);
        create("PICKAXE", 238);
        create("PICKAXE", 239);
        create("PICKAXE", 240);
        create("PICKAXE", 241);
        create("PICKAXE", 242);
        create("PICKAXE", 243);
        create("PICKAXE", 244);
        create("PICKAXE", 245);
        create("PICKAXE", 246);
        create("PICKAXE", 247);
        create("PICKAXE", 248);
        create("PICKAXE", 249);
        create("PICKAXE", 250);
        create("PICKAXE", 52);
        create("PICKAXE", 87);
        create("PICKAXE", 43, 6);
        create("PICKAXE", 44, 6);
        create("PICKAXE", 112);
        create("PICKAXE", 113);
        create("PICKAXE", 114);
        create("PICKAXE", 153);
        create("PICKAXE", 214);
        create("PICKAXE", 215);
        create("PICKAXE", 24);
        create("PICKAXE", 43, 1);
        create("PICKAXE", 44, 1);
        create("PICKAXE", 128);
        create("PICKAXE", 179);
        create("PICKAXE", 180);
        create("PICKAXE", 181);
        create("PICKAXE", 182);
        create("PICKAXE", 43, 0);
        create("PICKAXE", 43, 4);
        create("PICKAXE", 43, 5);
        create("PICKAXE", 43, 7);
        create("PICKAXE", 159);
        create("PICKAXE", 172);
        //========== "SHEARS" ==========\\
        create("SHEARS", 18);
        create("SHEARS", 161);
        create("SHEARS", 30);
        create("SHEARS", 35);
        //========== "SPADE" ==========\\
        create("SPADE", 82);
        create("SPADE", 2);
        create("SPADE", 3);
        create("SPADE", 252);
        create("SPADE", 60);
        create("SPADE", 13);
        create("SPADE", 110);
        create("SPADE", 12);
        create("SPADE", 88);
        //========== "SWORD" ==========\\
        create("SWORD", 127);
        create("SWORD", 91);
        create("SWORD", 86);
        create("SWORD", 18);
        create("SWORD", 161);
        create("SWORD", 106);
        create("SWORD", 103);
        create("SWORD", 30);
        //========== "ANY" ==========\\
        create("ANY", 69);
        create("ANY", 138);
        create("ANY", 160);
        create("ANY", 102);
        create("ANY", 95);
        create("ANY", 20);
        create("ANY", 89);
        create("ANY", 123);
        create("ANY", 124);
        create("ANY", 169);
        create("ANY", 81);
        create("ANY", 29);
        create("ANY", 33);
        create("ANY", 355);
        create("ANY", 26);
        create("ANY", 92);
        create("ANY", 171);
        create("ANY", 122);
        create("ANY", 170);
        create("ANY", 65);
        create("ANY", 144);
        create("ANY", 397);
        create("ANY", 97);
        create("ANY", 19);
        //========== "INSTA" ==========\\
        create("INSTA", 149);
        create("INSTA", 150);
        create("INSTA", 93);
        create("INSTA", 94);
        create("INSTA", 75);
        create("INSTA", 76);
        create("INSTA", 55);
        create("INSTA", 131);
        create("INSTA", 132);
        create("INSTA", 51);
        create("INSTA", 140);
        create("INSTA", 46);
        create("INSTA", 50);
        create("INSTA", 165);
        create("INSTA", 141);
        create("INSTA", 31);
        create("INSTA", 32);
        create("INSTA", 175);
        create("INSTA", 37);
        create("INSTA", 38);
        create("INSTA", 111);
        create("INSTA", 104);
        create("INSTA", 105);
        create("INSTA", 39);
        create("INSTA", 40);
        create("INSTA", 115);
        create("INSTA", 142);
        create("INSTA", 6);
        create("INSTA", 83);
        create("INSTA", 59);
        //========== "NONE" ==========\\
        create("NONE", 0);
        create("NONE", 10);
        create("NONE", 11);
        create("NONE", 8);
        create("NONE", 9);
        create("NONE", 7);
        create("NONE", 137);
        create("NONE", 210);
        create("NONE", 211);
        create("NONE", 166);
        create("NONE", 119);
        create("NONE", 120);
        create("NONE", 90);
    }

    private void create(String tool, int blockId, int blockData) {
        if (!getToolBreakBlocks().containsKey(tool))
            getToolBreakBlocks().put(tool, new ArrayList<>());

        getToolBreakBlocks().get(tool).add(new IdDataPair(blockId, blockData, false));
    }

    private void create(String tool, int blockId) {
        create(tool, blockId, -1);
    }

    public Boolean isProperTool(ItemStack tool, Block block) {
        if(tool == null)
            return false;

        if (tool.getType().name().contains("AXE") || tool.getType().name().contains("PICKAXE") || tool.getType().name().contains("SHEARS")
            || tool.getType().name().contains("SPADE") || tool.getType().name().contains("SWORD")) {
            if (tool.getType().name().contains("AXE"))
                for (IdDataPair blacklist : getToolBreakBlocks().get("AXE"))
                    if (blacklist.getId() == block.getTypeId() && (blacklist.getData() == -1 || blacklist.getData() == block.getData()))
                        return true;

            if (tool.getType().name().contains("PICKAXE"))
                for (IdDataPair blacklist : getToolBreakBlocks().get("PICKAXE"))
                    if (blacklist.getId() == block.getTypeId() && (blacklist.getData() == -1 || blacklist.getData() == block.getData()))
                        return true;

            if (tool.getType().name().contains("SHEARS"))
                for (IdDataPair blacklist : getToolBreakBlocks().get("SHEARS"))
                    if (blacklist.getId() == block.getTypeId() && (blacklist.getData() == -1 || blacklist.getData() == block.getData()))
                        return true;

            if (tool.getType().name().contains("SPADE"))
                for (IdDataPair blacklist : getToolBreakBlocks().get("SPADE"))
                    if (blacklist.getId() == block.getTypeId() && (blacklist.getData() == -1 || blacklist.getData() == block.getData()))
                        return true;

            if (tool.getType().name().contains("SWORD"))
                for (IdDataPair blacklist : getToolBreakBlocks().get("SWORD"))
                    if (blacklist.getId() == block.getTypeId() && (blacklist.getData() == -1 || blacklist.getData() == block.getData()))
                        return true;

            for (IdDataPair blacklist : getToolBreakBlocks().get("ANY"))
                if (blacklist.getId() == block.getTypeId() && (blacklist.getData() == -1 || blacklist.getData() == block.getData()))
                    return true;

            for (IdDataPair blacklist : getToolBreakBlocks().get("INSTA"))
                if (blacklist.getId() == block.getTypeId() && (blacklist.getData() == -1 || blacklist.getData() == block.getData()))
                    return true;
        }

        return false;
    }

    public Boolean isUnbreakable(int id, int data) {
        for (IdDataPair blacklist : getToolBreakBlocks().get("NONE"))
            if (blacklist.getId() == id && (blacklist.getData() == -1 || blacklist.getData() == data))
                return true;

        return false;
    }

    public HashMap<String, List<IdDataPair>> getToolBreakBlocks() {
        return toolBreakBlocks;
    }
}
