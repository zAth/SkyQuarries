package me.zath.skyquarries.controllers;
/*
 * MC 
 * Created by zAth
 */

import me.zath.skyquarries.Config;
import me.zath.skyquarries.SkyQuarries;
import me.zath.skyquarries.support.*;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SupportController {

    private enum Plugin {
        WorldGuard("WorldGuard", WorldGuardSupport.class),
        RedProtect("RedProtect", RedProtectSupport.class),
        PlotSquared("PlotSquared", PlotSquaredSupport.class),
        SkyBlock("SkyBlock", SkyBlockSupport.class);

        private String pluginName;
        private Class<? extends ISupportable> clazz;

        Plugin(String pluginName, Class<? extends ISupportable> clazz) {
            this.pluginName = pluginName;
            this.clazz = clazz;
        }

        public String getPluginName() {
            return pluginName;
        }

        public boolean canBuild(Player player, Block block){
            try {
                return (boolean) clazz.getMethod("canBuild", Player.class, Block.class).invoke(clazz.newInstance(), player, block);
            } catch (IllegalAccessException | NoSuchMethodException | InvocationTargetException | InstantiationException e) {
                e.printStackTrace();
            }

            return false;
        }
    }

    private List<Plugin> supportedPlugins;

    public SupportController(Config config) {
        supportedPlugins = new ArrayList<>();
        Arrays.stream(Plugin.values())
            .filter(plugin -> {
                boolean toSupport = config.getBoolean("Support." + plugin.name());
                if(!toSupport)
                    return false;

                if(SkyQuarries.getSkyQuarries().getServer().getPluginManager().getPlugin(plugin.getPluginName()) == null) {
                    SkyQuarries.getSkyQuarries().getServer().getLogger().severe("Não foi possivel ativar o suporte a " + plugin.getPluginName());
                    return false;
                }

                return true;
            }).forEach(plugin -> supportedPlugins.add(plugin));
    }

    public boolean canBuild(Player player, Block block){
        return !supportedPlugins.stream().filter(plugin -> !plugin.canBuild(player, block)).findAny().isPresent();
    }

}
