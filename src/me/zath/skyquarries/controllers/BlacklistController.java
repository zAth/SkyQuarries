package me.zath.skyquarries.controllers;
/*
 * MC 
 * Created by zAth
 */

import me.zath.skyquarries.SkyQuarries;
import me.zath.skyquarries.objects.IdDataPair;

import java.util.ArrayList;
import java.util.List;

public class BlacklistController {

    private List<IdDataPair> blackList;

    public BlacklistController() {
        blackList = new ArrayList<>();

        SkyQuarries.getSkyQuarries().getConfiguration().getBlackList().forEach(string -> {
            String idData = string;

            boolean toStop = false;
            if(string.contains(";")){
                toStop = Boolean.parseBoolean(string.split(";")[1]);
                idData = string.split(";")[0];
            }

            int id = 0;
            int data = 0;
            if (idData.contains(":")) {
                id = Integer.parseInt(idData.split(":")[0]);
                data = Integer.parseInt(idData.split(":")[1]);
            } else {
                id = Integer.parseInt(idData);
                data = -1;
            }

            create(id, data, toStop);
        });
    }

    public void create(int id, int data, boolean toStop) {
        blackList.add(new IdDataPair(id, data, toStop));
    }

    public boolean isInBlacklist(int id, int data) {
        if(SkyQuarries.getSkyQuarries().getToolBreakBlocksController().isUnbreakable(id, data))
            return true;

        if (blackList.isEmpty())
            return false;

        for (IdDataPair blacklist : blackList)
            if (blacklist.getId() == id && (blacklist.getData() == -1 || blacklist.getData() == data))
                return true;

        return false;
    }

    public boolean isStopper(int id, int data){
        for (IdDataPair blacklist : blackList)
            if (blacklist.getId() == id && (blacklist.getData() == -1 || blacklist.getData() == data) && blacklist.isToStop())
                return true;

        return false;
    }

    public List<IdDataPair> getBlackList() {
        return blackList;
    }

    @Override
    public String toString() {
        return "BlacklistController{" +
            "blackList=" + blackList +
            '}';
    }
}
