package me.zath.skyquarries.controllers;
/*
 * MC 
 * Created by zAth
 */

import com.sk89q.jnbt.*;
import me.zath.skyquarries.SkyQuarries;
import me.zath.skyquarries.objects.Schematic;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.zip.GZIPInputStream;

public class SchematicController {

    private HashMap<Integer, Schematic> schematicHashMap;

    public SchematicController() {
        schematicHashMap = new HashMap<>();

        for (File file : SkyQuarries.getSkyQuarries().getDataFolder().listFiles()) {
            if (!file.isDirectory()) {
                String fileName = file.getName();
                String fileExtension = "";

                int extensionIndex = fileName.lastIndexOf('.');
                int p = Math.max(fileName.lastIndexOf('/'), fileName.lastIndexOf('\\'));

                if (extensionIndex > p) {
                    fileExtension = fileName.substring(extensionIndex + 1);
                }

                if (!fileExtension.equals("schematic"))
                    continue;

                try {
                    Schematic schematic = loadSchematic(file);
                    if (schematic.getHeight() < 5){
                        SkyQuarries.getSkyQuarries().getServer().getLogger().severe("Não foi possivel carregar a schematic " + fileName + " porque" +
                            "a altura é menor que 5");
                        continue;
                    }
                    if (schematic.getLength() != schematic.getWidth()){
                        SkyQuarries.getSkyQuarries().getServer().getLogger().severe("Não foi possivel carregar a schematic " + fileName + " porque" +
                            "o comprimento é diferente da largura");
                        continue;
                    }
                    if (schematic.getLength() % 2 == 0){
                        SkyQuarries.getSkyQuarries().getServer().getLogger().severe("Não foi possivel carregar a schematic " + fileName + " porque" +
                            "a largura é um número par");
                        continue;
                    }

                    int x = schematic.getLength() / 2;
                    int z = schematic.getLength() - 1;
                    int index = z * schematic.getWidth() + x;

                    int id = schematic.getBlocks().clone()[index];
                    if (id < 0)
                        id += 256;

                    if(id != 154) {
                        SkyQuarries.getSkyQuarries().getServer().getLogger().severe("Não foi possivel carregar a schematic " + fileName + " porque" +
                            "o bloco mais abaixo do lado a sul da schematic, não um funil");
                        continue;
                    }

                    schematicHashMap.put((int) schematic.getLength() - 4, schematic);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private Schematic loadSchematic(File file) throws IOException {
        FileInputStream stream = new FileInputStream(file);
        NBTInputStream nbtStream = new NBTInputStream(new GZIPInputStream(stream));

        CompoundTag schematicTag = (CompoundTag) /*nbtStream.readTag*/nbtStream.readNamedTag().getTag();

        Map<String, Tag> schematic = schematicTag.getValue();
        if (!schematic.containsKey("Blocks")) {
            throw new IllegalArgumentException("Schematic file is missing a \"Blocks\" tag");
        }

        short width = getChildTag(schematic, "Width", ShortTag.class).getValue();
        short length = getChildTag(schematic, "Length", ShortTag.class).getValue();
        short height = getChildTag(schematic, "Height", ShortTag.class).getValue();

        String materials = getChildTag(schematic, "Materials", StringTag.class).getValue();
        if (!materials.equals("Alpha")) {
            throw new IllegalArgumentException("Schematic file is not an Alpha schematic");
        }

        byte[] blocks = getChildTag(schematic, "Blocks", ByteArrayTag.class).getValue();
        byte[] blockData = getChildTag(schematic, "Data", ByteArrayTag.class).getValue();
        return new Schematic(blocks, blockData, width, length, height);

    }

    private <T extends Tag> T getChildTag(Map<String, Tag> items, String key, Class<T> expected) throws IllegalArgumentException {
        if (!items.containsKey(key)) {
            throw new IllegalArgumentException("Schematic file is missing a \"" + key + "\" tag");
        }
        Tag tag = items.get(key);
        if (!expected.isInstance(tag)) {
            throw new IllegalArgumentException(key + " tag is not of tag type " + expected.getName());
        }
        return expected.cast(tag);
    }

    public HashMap<Integer, Schematic> getSchematicHashMap() {
        return schematicHashMap;
    }
}
