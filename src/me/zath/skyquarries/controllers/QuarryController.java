package me.zath.skyquarries.controllers;
/*
 * MC 
 * Created by zAth
 */

import me.zath.skyquarries.SkyQuarries;
import me.zath.skyquarries.objects.Quarry;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;

import java.util.HashMap;

public class QuarryController {

    private HashMap<Location, Quarry> quarryHashMap;

    public QuarryController() {
        quarryHashMap = new HashMap<>();
    }

    public void create(Player player, Block block, int holeSize) {
        String cardinalDirection = SkyQuarries.getSkyQuarries().getUtils().getCardinalDirection(player.getLocation().getYaw());

        SkyQuarries.getSkyQuarries().getSchematicController().getSchematicHashMap().get(holeSize).assemble(cardinalDirection, SkyQuarries.getSkyQuarries().getUtils().getPasteLocation(cardinalDirection, block.getLocation(), holeSize), false);
        Quarry quarry = new Quarry(player.getName().toLowerCase(), holeSize, SkyQuarries.getSkyQuarries().getUtils().serializeLocation(block.getLocation())
            , cardinalDirection, false);

        quarryHashMap.put(quarry.getFurnaceLocation(), quarry);

        SkyQuarries.getSkyQuarries().getSql().createQuarry(quarry);
    }

    public void destroy(Quarry quarry) {
        quarry.sendBlockBreak(-1);
        quarry.getLaser().despawn(null);
        if(quarry.getArmorStand() != null)
            quarry.getArmorStand().remove();
        quarry.getFurnace().getInventory().setFuel(null);
        quarry.getFurnace().getInventory().setSmelting(null);

        Location pasteLocation = SkyQuarries.getSkyQuarries().getUtils().getPasteLocation(quarry.getDirection(), quarry.getFurnaceLocation(), quarry.getHoleSize());
        SkyQuarries.getSkyQuarries().getSchematicController().getSchematicHashMap().get(quarry.getHoleSize()).assemble(quarry.getDirection(), pasteLocation, true);

        quarryHashMap.remove(quarry.getFurnaceLocation());
        SkyQuarries.getSkyQuarries().getSql().deleteQuarry(quarry);
    }

    public Boolean can(Location locationToCheck) {
        for (Quarry quarry : quarryHashMap.values()) {
            Location location1 = quarry.getQuarryMin().clone();
            Location location2 = quarry.getQuarryMax().clone();

            if (locationToCheck.getWorld() != location1.getWorld()) {
                continue;
            }

            if (location1.getBlockX() > location2.getBlockX()) {
                int minX = location1.getBlockX();
                location1.setX(location2.getBlockX());
                location2.setX(minX);
            }
            if (location1.getBlockY() > location2.getBlockY()) {
                int minY = location1.getBlockY();
                location1.setY(location2.getBlockY());
                location2.setY(minY);
            }
            if (location1.getBlockZ() > location2.getBlockZ()) {
                int minZ = location1.getBlockZ();
                location1.setZ(location2.getBlockZ());
                location2.setZ(minZ);
            }

            for (int x = location1.getBlockX(); x <= location2.getBlockX(); x++) {
                for (int y = location1.getBlockY(); y <= location2.getBlockY(); y++) {
                    for (int z = location1.getBlockZ(); z <= location2.getBlockZ(); z++) {
                        Location location = new Location(locationToCheck.getWorld(), x, y, z);
                        if (location.equals(locationToCheck)) {
                            return false;
                        }

                    }
                }
            }
        }

        return true;
    }

    public Boolean can(Location locationToCheck, Player player) {
        for (Quarry quarry : quarryHashMap.values()) {
            Location location1 = quarry.getQuarryMin().clone();
            Location location2 = quarry.getQuarryMax().clone();

            if (locationToCheck.getWorld() != location1.getWorld()) {
                continue;
            }

            if (location1.getBlockX() > location2.getBlockX()) {
                int minX = location1.getBlockX();
                location1.setX(location2.getBlockX());
                location2.setX(minX);
            }
            if (location1.getBlockY() > location2.getBlockY()) {
                int minY = location1.getBlockY();
                location1.setY(location2.getBlockY());
                location2.setY(minY);
            }
            if (location1.getBlockZ() > location2.getBlockZ()) {
                int minZ = location1.getBlockZ();
                location1.setZ(location2.getBlockZ());
                location2.setZ(minZ);
            }

            for (int x = location1.getBlockX(); x <= location2.getBlockX(); x++) {
                for (int y = location1.getBlockY(); y <= location2.getBlockY(); y++) {
                    for (int z = location1.getBlockZ(); z <= location2.getBlockZ(); z++) {
                        Location location = new Location(locationToCheck.getWorld(), x, y, z);
                        if (location.equals(locationToCheck) && !quarry.getOwnerName().equalsIgnoreCase(player.getName())) {
                            return false;
                        }
                    }
                }
            }
        }

        return true;
    }

    public HashMap<Location, Quarry> getQuarryHashMap() {
        return quarryHashMap;
    }
}
