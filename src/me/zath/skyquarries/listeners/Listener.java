package me.zath.skyquarries.listeners;
/*
 * MC 
 * Created by zAth
 */

import me.zath.skyquarries.SkyQuarries;
import me.zath.skyquarries.objects.Quarry;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.block.Furnace;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.*;
import org.bukkit.event.entity.EntityChangeBlockEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.FurnaceInventory;
import org.bukkit.inventory.ItemStack;

import java.util.HashMap;
import java.util.concurrent.TimeUnit;

public class Listener implements org.bukkit.event.Listener {

    private HashMap<String, Long> cooldown = new HashMap<>();

    @EventHandler
    public void quarryClickEvent(PlayerInteractEvent e) {
        if (e.getAction() == Action.RIGHT_CLICK_BLOCK) {
            if (SkyQuarries.getSkyQuarries().getQuarryController().getQuarryHashMap().containsKey(e.getClickedBlock().getLocation())) {
                if (!e.getPlayer().hasPermission("skyquarries.bypass") && !SkyQuarries.getSkyQuarries().getQuarryController().getQuarryHashMap().get(e.getClickedBlock().getLocation()).getOwnerName().equalsIgnoreCase(e.getPlayer().getName())) {
                    e.setCancelled(true);
                    e.getPlayer().sendMessage(SkyQuarries.getSkyQuarries().getConfiguration().getMsg_NotOwner());
                    return;
                }
                e.setCancelled(false);
                e.getPlayer().playSound(e.getPlayer().getLocation(), Sound.CHEST_OPEN, 1, 1);
            } else {
                if (!e.getPlayer().hasPermission("skyquarries.bypass") && !SkyQuarries.getSkyQuarries().getQuarryController().can(e.getClickedBlock().getLocation(), e.getPlayer())) {
                    e.setCancelled(true);
                    e.getPlayer().sendMessage(SkyQuarries.getSkyQuarries().getConfiguration().getMsg_NotOwner());
                }
            }
        }
    }

    @EventHandler
    public void quarryOpenEvent(InventoryClickEvent e) {
        if (e.getInventory().getType() != InventoryType.FURNACE) return;

        FurnaceInventory furnaceInventory = (FurnaceInventory) e.getInventory();
        Furnace furnace = furnaceInventory.getHolder();
        if (!SkyQuarries.getSkyQuarries().getQuarryController().getQuarryHashMap().containsKey(furnace.getLocation())) return;

        Quarry quarry = SkyQuarries.getSkyQuarries().getQuarryController().getQuarryHashMap().get(furnace.getLocation());

        if (e.getRawSlot() < furnaceInventory.getSize()) {
            e.setCancelled(true);

            ItemStack itemStack = e.getCurrentItem();
            if (e.getSlot() == 0) {
                quarry.setEnabled(!quarry.isEnabled());
                quarry.setSecondsGone(0.0);
                quarry.sendBlockBreak(-1);
                if(quarry.getArmorStand() != null)
                    quarry.getArmorStand().remove();
                quarry.getLaser().despawn(null);

                ItemStack button = quarry.isEnabled()
                    ? SkyQuarries.getSkyQuarries().getConfiguration().getItems().getItemStack_On()
                    : SkyQuarries.getSkyQuarries().getConfiguration().getItems().getItemStack_Off();
                furnaceInventory.setSmelting(button);
            } else if (e.getSlot() == 1) {
                SkyQuarries.getSkyQuarries().getQuarryController().destroy(quarry);
                if(e.getWhoClicked().hasPermission("skyquarries.destroy")){
                    ItemStack items = SkyQuarries.getSkyQuarries().getConfiguration().getItems().getItemStack_Quarry(quarry.getHoleSize());
                    HashMap<Integer, ItemStack> map = e.getWhoClicked().getInventory().addItem(items);
                    if(!map.isEmpty())
                        map.values().forEach(_itemStack ->
                            e.getWhoClicked().getWorld().dropItemNaturally(e.getWhoClicked().getLocation(), _itemStack));
                }
            } else if (e.getSlot() == 2) {
                if (e.getCursor() != null) {
                    if (itemStack != null) {
                        furnaceInventory.setResult(e.getCursor());
                        e.setCursor(itemStack);
                    } else {
                        furnaceInventory.setResult(e.getCursor());
                    }
                } else {
                    if (itemStack != null) {
                        e.setCursor(itemStack);
                    }
                }
                quarry.setSecondsGone(0.0);
                quarry.setSecondsToBreak(SkyQuarries.getSkyQuarries().getUtils().getSecondsToBreak(furnaceInventory.getResult(), quarry.getBlock()));
            }
        }
    }

    @EventHandler
    public void blockPlaceEvent(BlockPlaceEvent e) {
        if(!SkyQuarries.getSkyQuarries().getConfiguration().getQuarry_WorldWhitelist().isEmpty() && !SkyQuarries.getSkyQuarries().getConfiguration().getQuarry_WorldWhitelist().contains(e.getPlayer().getWorld().getName()))
            return;

        Location newQuarryMin = e.getBlockPlaced().getLocation().clone();
        Location newQuarryMax;
        Boolean wantsToCreate = e.getPlayer().getItemInHand() != null
            && SkyQuarries.getSkyQuarries().getConfiguration().getItems().isItemStack_Quarry(e.getPlayer().getItemInHand());

        if (wantsToCreate) {

            if(!e.getPlayer().hasPermission("skyquarries.bypass")) {
                Long delay = TimeUnit.SECONDS.toMillis(SkyQuarries.getSkyQuarries().getConfiguration().getCooldown());
                if (cooldown.keySet().contains(e.getPlayer().getName().toLowerCase())) {
                    if (cooldown.get(e.getPlayer().getName().toLowerCase()) > System.currentTimeMillis()) {
                        e.getPlayer().sendMessage(SkyQuarries.getSkyQuarries().getConfiguration().getMsg_InCooldown());
                        e.setCancelled(true);
                        return;
                    }
                    cooldown.put(e.getPlayer().getName().toLowerCase(), System.currentTimeMillis() + delay);
                }
                cooldown.put(e.getPlayer().getName().toLowerCase(), System.currentTimeMillis() + delay);
            }

            String direction = SkyQuarries.getSkyQuarries().getUtils().getCardinalDirection(e.getPlayer().getLocation().getYaw());
            int holeSize = SkyQuarries.getSkyQuarries().getConfiguration().getItems().getItemStack_QuarryHoleSize(e.getPlayer().getItemInHand());

            newQuarryMin
                .add(SkyQuarries.getSkyQuarries().getUtils().getLeftSide(direction).clone()
                    .multiply(Math.floor(SkyQuarries.getSkyQuarries().getSchematicController().getSchematicHashMap().get(holeSize).getLength() / 2))
                    .setY(-newQuarryMin.getY() + 1));

            newQuarryMax = newQuarryMin.clone()
                .add(SkyQuarries.getSkyQuarries().getUtils().getDirection(direction).clone()
                    .multiply(SkyQuarries.getSkyQuarries().getSchematicController().getSchematicHashMap().get(holeSize).getLength() - 1))
                .add(SkyQuarries.getSkyQuarries().getUtils().getRighttSide(direction).clone()
                    .multiply(SkyQuarries.getSkyQuarries().getSchematicController().getSchematicHashMap().get(holeSize).getLength() - 1)
                    .setY(255 - newQuarryMin.getY()));

            if (newQuarryMin.getBlockX() > newQuarryMax.getBlockX()) {
                int minX = newQuarryMin.getBlockX();
                newQuarryMin.setX(newQuarryMax.getBlockX());
                newQuarryMax.setX(minX);
            }
            if (newQuarryMin.getBlockY() > newQuarryMax.getBlockY()) {
                int minY = newQuarryMin.getBlockY();
                newQuarryMin.setY(newQuarryMax.getBlockY());
                newQuarryMax.setY(minY);
            }
            if (newQuarryMin.getBlockZ() > newQuarryMax.getBlockZ()) {
                int minZ = newQuarryMin.getBlockZ();
                newQuarryMin.setZ(newQuarryMax.getBlockZ());
                newQuarryMax.setZ(minZ);
            }

            if (!SkyQuarries.getSkyQuarries().getQuarryController().getQuarryHashMap().isEmpty()) {
                for (int x = newQuarryMin.getBlockX(); x <= newQuarryMax.getBlockX(); x++) {
                    for (int y = newQuarryMin.getBlockY(); y <= newQuarryMax.getBlockY(); y++) {
                        for (int z = newQuarryMin.getBlockZ(); z <= newQuarryMax.getBlockZ(); z++) {
                            Block block = new Location(e.getBlockPlaced().getWorld(), x, y, z).getBlock();
                            if(!SkyQuarries.getSkyQuarries().getQuarryController().can(block.getLocation())
                                || !SkyQuarries.getSkyQuarries().getSupportController().canBuild(e.getPlayer(), block)) {
                                e.getPlayer().sendMessage(SkyQuarries.getSkyQuarries().getConfiguration().getMsg_QuarryNear());
                                e.setCancelled(true);
                                return;
                            }
                        }
                    }
                }

                SkyQuarries.getSkyQuarries().getQuarryController().create(e.getPlayer(), e.getBlockPlaced(), holeSize);
            } else {
                for (int x = newQuarryMin.getBlockX(); x <= newQuarryMax.getBlockX(); x++) {
                    for (int y = newQuarryMin.getBlockY(); y <= newQuarryMax.getBlockY(); y++) {
                        for (int z = newQuarryMin.getBlockZ(); z <= newQuarryMax.getBlockZ(); z++) {
                            Block block = new Location(e.getBlockPlaced().getWorld(), x, y, z).getBlock();
                            if(!SkyQuarries.getSkyQuarries().getSupportController().canBuild(e.getPlayer(), block)) {
                                e.getPlayer().sendMessage(SkyQuarries.getSkyQuarries().getConfiguration().getMsg_QuarryNear());
                                e.setCancelled(true);
                                return;
                            }
                        }
                    }
                }

                SkyQuarries.getSkyQuarries().getQuarryController().create(e.getPlayer(), e.getBlockPlaced(), holeSize);
            }
        } else {
            if (!SkyQuarries.getSkyQuarries().getQuarryController().getQuarryHashMap().isEmpty()) {
                if (!e.getPlayer().hasPermission("skyquarries.bypass") && !SkyQuarries.getSkyQuarries().getQuarryController().can(e.getBlock().getLocation(), e.getPlayer())
                    && !SkyQuarries.getSkyQuarries().getQuarryController().can(e.getBlock().getLocation())) {
                    e.getPlayer().sendMessage(SkyQuarries.getSkyQuarries().getConfiguration().getMsg_QuarryNear());
                    e.setCancelled(true);
                }
            }
        }
    }

    @EventHandler
    public void blockDamageEvent(BlockDamageEvent e) {
        if (!SkyQuarries.getSkyQuarries().getQuarryController().can(e.getBlock().getLocation()))
            e.setCancelled(true);
    }

    @EventHandler
    public void blockExplodeEvent(BlockExplodeEvent e) {
        e.blockList().forEach(block -> {
            if (SkyQuarries.getSkyQuarries().getQuarryController().can(block.getLocation()))
                e.setCancelled(true);
        });
    }

    @EventHandler
    public void blockIgniteEvent(BlockIgniteEvent e) {
        if (!SkyQuarries.getSkyQuarries().getQuarryController().can(e.getBlock().getLocation()))
            e.setCancelled(true);
    }

    @EventHandler
    public void blockBurnEvent(BlockBurnEvent e) {
        if (!SkyQuarries.getSkyQuarries().getQuarryController().can(e.getBlock().getLocation()))
            e.setCancelled(true);
    }

    @EventHandler
    public void blockBreakEvent(BlockBreakEvent e) {
        if (!e.getPlayer().hasPermission("skyquarries.bypass") && !SkyQuarries.getSkyQuarries().getQuarryController().can(e.getBlock().getLocation(), e.getPlayer())) {
            e.getPlayer().sendMessage(SkyQuarries.getSkyQuarries().getConfiguration().getMsg_QuarryNear());
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void entityChangeBlockEvent(EntityChangeBlockEvent e) {
        if (!SkyQuarries.getSkyQuarries().getQuarryController().can(e.getBlock().getLocation()))
            e.setCancelled(true);
    }

    @EventHandler
    public void entityExplodeEvent(EntityExplodeEvent e) {
        e.blockList().forEach(block -> {
            if (SkyQuarries.getSkyQuarries().getQuarryController().can(block.getLocation()))
                e.setCancelled(true);
        });
    }

    @EventHandler
    public void blockSpreadEvent(BlockSpreadEvent e) {
        if (e.getSource().getType() != Material.FIRE) return;

        if (!SkyQuarries.getSkyQuarries().getQuarryController().can(e.getBlock().getLocation()))
            e.setCancelled(true);
    }

    @EventHandler
    public void blockPistonRetractEvent(BlockPistonRetractEvent e) {
        e.getBlocks().forEach(block -> {
            if (SkyQuarries.getSkyQuarries().getQuarryController().can(block.getLocation()))
                e.setCancelled(true);
        });
    }

}
