package me.zath.skyquarries.utils;
/*
 * MC 
 * Created by zAth
 */

import me.zath.skyquarries.SkyQuarries;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

public class ItemBuilder {

    private ItemStack itemStack;

    public ItemBuilder(int id, int data) {
        this.itemStack = new ItemStack(id, 1, (short) data);
    }

    public ItemBuilder(ItemStack itemStack) {
        this.itemStack = itemStack;
    }

    private ItemMeta getItemMeta() {
        return this.itemStack.getItemMeta();
    }

    private ItemBuilder setItemMeta(ItemMeta itemMeta) {
        this.itemStack.setItemMeta(itemMeta);
        return this;
    }

    public ItemBuilder withName(String displayName) {
        ItemMeta itemMeta = getItemMeta();
        itemMeta.setDisplayName(displayName);
        return setItemMeta(itemMeta);
    }

    public ItemBuilder withLore(List<String> lore) {
        ItemMeta itemMeta = getItemMeta();
        itemMeta.setLore(lore);
        return setItemMeta(itemMeta);
    }

    public ItemBuilder withGlow(Boolean toggle) {
        if (toggle) {
            try {
                Object nmsItem = SkyQuarries.getSkyQuarries().getReflection().getMethod_asNMSCopy().invoke(null, this.itemStack);
                Object nbtTagCompound = null;

                if(!(boolean) SkyQuarries.getSkyQuarries().getReflection().getMethod_HasTag().invoke(nmsItem)){
                    nbtTagCompound = SkyQuarries.getSkyQuarries().getReflection().getConstructor_NBTTagCompound().newInstance();
                    SkyQuarries.getSkyQuarries().getReflection().getMethod_SetTag().invoke(nmsItem, nbtTagCompound);
                }

                if(nbtTagCompound == null){
                    nbtTagCompound = SkyQuarries.getSkyQuarries().getReflection().getMethod_GetTag().invoke(nmsItem);
                }

                Object nbtTagList = SkyQuarries.getSkyQuarries().getReflection().getConstructor_NBTTagList().newInstance();
                SkyQuarries.getSkyQuarries().getReflection().getMethod_Set().invoke(nbtTagCompound, "ench", nbtTagList);
                SkyQuarries.getSkyQuarries().getReflection().getMethod_SetTag().invoke(nmsItem, nbtTagCompound);

                this.itemStack = (ItemStack) SkyQuarries.getSkyQuarries().getReflection().getMethod_AsCraftMirror().invoke(null, nmsItem);
            } catch (IllegalAccessException | InvocationTargetException | InstantiationException e) {
                e.printStackTrace();
            }
        }

        return this;
    }

    public ItemStack build() {
        return this.itemStack;
    }
}
