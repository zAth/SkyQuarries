package me.zath.skyquarries.utils;
/*
 * MC
 * Created by zAth
 */

import me.zath.skyquarries.SkyQuarries;
import me.zath.skyquarries.objects.Quarry;

import java.io.File;
import java.sql.*;

public class SQL {

    private Connection connection;
    private File db;

    public SQL(File db) {
        this.db = db;
        createTable();
        loadQuarries();
    }

    public void closeConnection() {
        try {
            if (connection == null || connection.isClosed()) return;

            connection.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void openConnection() {
        try {
            if (connection != null && !connection.isClosed()) return;

            Class.forName("org.sqlite.JDBC");
            connection = DriverManager.getConnection("jdbc:sqlite:" + db.getAbsolutePath());
        } catch (ClassNotFoundException | SQLException ex8) {
            ex8.printStackTrace();
        }
    }

    public void createTable() {
        openConnection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement
                ("CREATE TABLE IF NOT EXISTS quarries(ownerName varchar(255), holeSize tinyint(1), serializedFurnaceLocation varchar(255)" +
                    ", direction char(1), enabled boolean);");
            
            preparedStatement.executeUpdate();
            preparedStatement.close();
        } catch (SQLException ex) {
            if(SkyQuarries.getSkyQuarries().getConfiguration().getSql_ShowPossibleSqlErrors()) ex.printStackTrace();
            
            SkyQuarries.getSkyQuarries().getServer().getConsoleSender().sendMessage(SkyQuarries.getSkyQuarries().getDescription().getName() + "§4Erro criando a tabela");
            SkyQuarries.getSkyQuarries().getServer().getConsoleSender().sendMessage(SkyQuarries.getSkyQuarries().getDescription().getName() + "§4Criando a tabela novamente...");
            createTable();
        }
    }

    public void createQuarry(Quarry quarry) {
        openConnection();
        try {
            PreparedStatement statement = connection.prepareStatement("INSERT INTO quarries values(?,?,?,?,?);");
            statement.setString(1, quarry.getOwnerName());
            statement.setInt(2, quarry.getHoleSize());
            statement.setString(3, SkyQuarries.getSkyQuarries().getUtils().serializeLocation(quarry.getFurnaceLocation()));
            statement.setString(4, quarry.getDirection());
            statement.setBoolean(5, quarry.isEnabled());
            statement.execute();
            statement.close();
        } catch (Exception e) {
            if(SkyQuarries.getSkyQuarries().getConfiguration().getSql_ShowPossibleSqlErrors()) e.printStackTrace();
            
            SkyQuarries.getSkyQuarries().getServer().getConsoleSender().sendMessage(SkyQuarries.getSkyQuarries().getDescription().getName() + "§4Erro criando a mineradora");
            SkyQuarries.getSkyQuarries().getServer().getConsoleSender().sendMessage(SkyQuarries.getSkyQuarries().getDescription().getName() + quarry.toString());
            SkyQuarries.getSkyQuarries().getServer().getConsoleSender().sendMessage(SkyQuarries.getSkyQuarries().getDescription().getName() + "§4Criando novamente...");
            createQuarry(quarry);
        }
    }

    public void deleteQuarry(Quarry quarry) {
        openConnection();
        try {
            PreparedStatement statement = connection.prepareStatement("DELETE FROM quarries WHERE serializedFurnaceLocation=?;");
            statement.setString(1, SkyQuarries.getSkyQuarries().getUtils().serializeLocation(quarry.getFurnaceLocation()));

            statement.execute();
            statement.close();
        } catch (Exception e) {
            if(SkyQuarries.getSkyQuarries().getConfiguration().getSql_ShowPossibleSqlErrors()) e.printStackTrace();

            SkyQuarries.getSkyQuarries().getServer().getConsoleSender().sendMessage(SkyQuarries.getSkyQuarries().getDescription().getName() + "§4Erro deletando a mineradora");
            SkyQuarries.getSkyQuarries().getServer().getConsoleSender().sendMessage(SkyQuarries.getSkyQuarries().getDescription().getName() + quarry.toString());
            SkyQuarries.getSkyQuarries().getServer().getConsoleSender().sendMessage(SkyQuarries.getSkyQuarries().getDescription().getName() + "§4Deletando novamente...");
            deleteQuarry(quarry);
        }
    }

    public void loadQuarries() {
        openConnection();
        try {
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM quarries;");

            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                String ownerName = rs.getString("ownerName");
                int holeSize = rs.getInt("holeSize");
                String serializedFurnaceLocation = rs.getString("serializedFurnaceLocation");
                String direction = rs.getString("direction");
                boolean enabled = rs.getBoolean("enabled");

                Quarry quarry = new Quarry(ownerName, holeSize, serializedFurnaceLocation, direction, enabled);
                SkyQuarries.getSkyQuarries().getQuarryController().getQuarryHashMap().put(quarry.getFurnaceLocation(), quarry);
            }

            statement.close();
            rs.close();
        } catch (Exception e) {
            if(SkyQuarries.getSkyQuarries().getConfiguration().getSql_ShowPossibleSqlErrors()) e.printStackTrace();
            
            SkyQuarries.getSkyQuarries().getServer().getConsoleSender().sendMessage(SkyQuarries.getSkyQuarries().getDescription().getName() + "§4Erro carregando as mineradoras");
            SkyQuarries.getSkyQuarries().getServer().getConsoleSender().sendMessage(SkyQuarries.getSkyQuarries().getDescription().getName() + "§4Carregando novamente...");
            loadQuarries();
        }
    }

    public void updateQuarry(Quarry quarry) {
        openConnection();
        try {
            PreparedStatement statement = connection.prepareStatement("UPDATE quarries SET enabled=? WHERE serializedFurnaceLocation=?");

            statement.setBoolean(1, quarry.isEnabled());
            statement.setString(2, SkyQuarries.getSkyQuarries().getUtils().serializeLocation(quarry.getFurnaceLocation()));

            statement.execute();
            statement.close();
        } catch (Exception e) {
            if(SkyQuarries.getSkyQuarries().getConfiguration().getSql_ShowPossibleSqlErrors()) e.printStackTrace();
            
            SkyQuarries.getSkyQuarries().getServer().getConsoleSender().sendMessage(SkyQuarries.getSkyQuarries().getDescription().getName() + "§4Erro atualizando a mineradora");
            SkyQuarries.getSkyQuarries().getServer().getConsoleSender().sendMessage(SkyQuarries.getSkyQuarries().getDescription().getName() + quarry.toString());
            SkyQuarries.getSkyQuarries().getServer().getConsoleSender().sendMessage(SkyQuarries.getSkyQuarries().getDescription().getName() + "§4Atualizando novamente...");
            updateQuarry(quarry);
        }
    }

}
