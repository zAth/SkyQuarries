package me.zath.skyquarries.utils;
/*
 * MC 
 * Created by zAth
 */

import me.zath.skyquarries.SkyQuarries;
import org.bukkit.inventory.ItemStack;

import java.lang.reflect.InvocationTargetException;
import java.util.stream.Collectors;

public class Items {

    private ItemStack itemStack_On, itemStack_Off, itemStack_Destroy, itemStack_Quarry;

    public ItemStack getItemStack_Quarry(int holeSize) {
        ItemStack itemStack = new ItemBuilder(itemStack_Quarry.clone())
            .withName(itemStack_Quarry.getItemMeta().getDisplayName().replaceAll("%holeSize%", "" + holeSize))
            .withLore(itemStack_Quarry.getItemMeta().getLore().stream()
                .map(string -> string.replaceAll("%holeSize%", "" + holeSize))
                .collect(Collectors.toList()))
            .build();

        try {
            Object nmsItem = SkyQuarries.getSkyQuarries().getReflection().getMethod_asNMSCopy().invoke(null, itemStack);
            Object nbtTagCompound;

            if (!(boolean) SkyQuarries.getSkyQuarries().getReflection().getMethod_HasTag().invoke(nmsItem)) {
                nbtTagCompound = SkyQuarries.getSkyQuarries().getReflection().getConstructor_NBTTagCompound().newInstance();
                SkyQuarries.getSkyQuarries().getReflection().getMethod_SetTag().invoke(nmsItem, nbtTagCompound);
            } else {
                nbtTagCompound = SkyQuarries.getSkyQuarries().getReflection().getMethod_GetTag().invoke(nmsItem);
            }

            SkyQuarries.getSkyQuarries().getReflection().getMethod_SetInt().invoke(nbtTagCompound, "holeSize", holeSize);
            SkyQuarries.getSkyQuarries().getReflection().getMethod_SetTag().invoke(nmsItem, nbtTagCompound);

            itemStack = (ItemStack) SkyQuarries.getSkyQuarries().getReflection().getMethod_AsCraftMirror().invoke(null, nmsItem);
        } catch (IllegalAccessException | InvocationTargetException | InstantiationException e) {
            e.printStackTrace();
        }

        return itemStack;
    }

    public boolean isItemStack_Quarry(ItemStack itemStack) {
        try {
            Object nmsItem = SkyQuarries.getSkyQuarries().getReflection().getMethod_asNMSCopy().invoke(null, itemStack);
            Object nbtTagCompound;

            if (!(boolean) SkyQuarries.getSkyQuarries().getReflection().getMethod_HasTag().invoke(nmsItem)) {
                nbtTagCompound = SkyQuarries.getSkyQuarries().getReflection().getConstructor_NBTTagCompound().newInstance();
                SkyQuarries.getSkyQuarries().getReflection().getMethod_SetTag().invoke(nmsItem, nbtTagCompound);
            } else {
                nbtTagCompound = SkyQuarries.getSkyQuarries().getReflection().getMethod_GetTag().invoke(nmsItem);
            }

            return (boolean) SkyQuarries.getSkyQuarries().getReflection().getMethod_HasKey().invoke(nbtTagCompound, "holeSize");
        } catch (IllegalAccessException | InvocationTargetException | InstantiationException e) {
            e.printStackTrace();
        }

        return false;
    }

    public int getItemStack_QuarryHoleSize(ItemStack itemStack) {
        try {
            Object nmsItem = SkyQuarries.getSkyQuarries().getReflection().getMethod_asNMSCopy().invoke(null, itemStack);
            Object nbtTagCompound;

            if (!(boolean) SkyQuarries.getSkyQuarries().getReflection().getMethod_HasTag().invoke(nmsItem)) {
                nbtTagCompound = SkyQuarries.getSkyQuarries().getReflection().getConstructor_NBTTagCompound().newInstance();
                SkyQuarries.getSkyQuarries().getReflection().getMethod_SetTag().invoke(nmsItem, nbtTagCompound);
            } else {
                nbtTagCompound = SkyQuarries.getSkyQuarries().getReflection().getMethod_GetTag().invoke(nmsItem);
            }

            return (int) SkyQuarries.getSkyQuarries().getReflection().getMethod_GetInt().invoke(nbtTagCompound, "holeSize");
        } catch (IllegalAccessException | InvocationTargetException | InstantiationException e) {
            e.printStackTrace();
        }

        return 0;
    }

    public void setItemStack_Quarry(ItemStack itemStack_Quarry) {
        this.itemStack_Quarry = itemStack_Quarry;
    }

    public ItemStack getItemStack_On() {
        return itemStack_On;
    }

    public void setItemStack_On(ItemStack itemStack_On) {
        this.itemStack_On = itemStack_On;
    }

    public ItemStack getItemStack_Off() {
        return itemStack_Off;
    }

    public void setItemStack_Off(ItemStack itemStack_Off) {
        this.itemStack_Off = itemStack_Off;
    }

    public ItemStack getItemStack_Destroy() {
        return itemStack_Destroy;
    }

    public void setItemStack_Destroy(ItemStack itemStack_Destroy) {
        this.itemStack_Destroy = itemStack_Destroy;
    }

}
