package me.zath.skyquarries.utils;
/*
 * MC
 * Created by zAth
 */

import me.zath.skyquarries.SkyQuarries;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class Reflection {

    private String versionPrefix = "";
    private Class<?> class_ItemStack, class_CraftItemStack
        , class_PacketPlayOutSpawnEntityLiving, class_PlayerConnection, class_Packet
        , class_PacketPlayOutEntityDestroy, class_World, class_DataWatcher, class_CraftWorld, class_Entity, class_CraftEntity
        , class_PacketPlayOutEntityMetadata, class_PacketPlayOutBlockBreakAnimation, class_BlockPosition, class_CraftServer
        , class_PlayerList, class_CraftMagicNumbers, class_Block, class_NBTTagCompound, class_NBTTagList, class_NBTBase;
    private Method method_asNMSCopy, method_SendPacket, method_GetId, method_GetHandle, method_CraftEntityGetHandle
        , method_DWA , method_DWB, method_CraftServerGetHandle, method_SendPacketNearby, method_GetBlock, method_G, method_A, method_HasTag
        , method_SetTag, method_GetTag, method_Set, method_AsCraftMirror, method_SetInt, method_HasKey, method_GetInt;
    private Constructor<?> constructor_PacketPlayOutSpawnEntityLiving, constructor_PacketPlayOutEntityDestroy
        , constructor_PacketPlayOutEntityMetadata, constructor_DataWatcher, constructor_PacketPlayOutBlockBreakAnimation, constructor_BlockPosition
        , constructor_NBTTagCompound, constructor_NBTTagList;

    public Reflection() {
        loadClasses();
        loadMethods();
        loadConstructors();
    }

    private void loadClasses(){
        try {
            String className = SkyQuarries.getSkyQuarries().getServer().getClass().getName();
            String[] packages = className.split("\\.");
            if (packages.length == 5) {
                versionPrefix = packages[3] + ".";
            }

            class_ItemStack = fixBukkitClass("net.minecraft.server.ItemStack");
            class_CraftItemStack = fixBukkitClass("org.bukkit.craftbukkit.inventory.CraftItemStack");
            class_PacketPlayOutSpawnEntityLiving = fixBukkitClass("net.minecraft.server.PacketPlayOutSpawnEntityLiving");
            class_PlayerConnection = fixBukkitClass("net.minecraft.server.PlayerConnection");
            class_Packet = fixBukkitClass("net.minecraft.server.Packet");
            class_PacketPlayOutEntityDestroy = fixBukkitClass("net.minecraft.server.PacketPlayOutEntityDestroy");
            class_World = fixBukkitClass("net.minecraft.server.World");
            class_CraftWorld = fixBukkitClass("org.bukkit.craftbukkit.CraftWorld");
            class_Entity = fixBukkitClass("net.minecraft.server.Entity");
            class_CraftEntity = fixBukkitClass("org.bukkit.craftbukkit.entity.CraftEntity");
            class_PacketPlayOutEntityMetadata = fixBukkitClass("net.minecraft.server.PacketPlayOutEntityMetadata");
            class_DataWatcher = fixBukkitClass("net.minecraft.server.DataWatcher");
            class_PacketPlayOutBlockBreakAnimation = fixBukkitClass("net.minecraft.server.PacketPlayOutBlockBreakAnimation");
            class_BlockPosition = fixBukkitClass("net.minecraft.server.BlockPosition");
            class_PlayerList = fixBukkitClass("net.minecraft.server.PlayerList");
            class_CraftServer = fixBukkitClass("org.bukkit.craftbukkit.CraftServer");
            class_CraftMagicNumbers = fixBukkitClass("org.bukkit.craftbukkit.util.CraftMagicNumbers");
            class_Block = fixBukkitClass("net.minecraft.server.Block");
            class_NBTTagCompound = fixBukkitClass("net.minecraft.server.NBTTagCompound");
            class_NBTTagList = fixBukkitClass("net.minecraft.server.NBTTagList");
            class_NBTBase = fixBukkitClass("net.minecraft.server.NBTBase");
        } catch (Throwable ex) {
            ex.printStackTrace();
        }
    }

    private void loadMethods(){
        try {
            method_asNMSCopy = class_CraftItemStack.getMethod("asNMSCopy", org.bukkit.inventory.ItemStack.class);
            method_SendPacket = class_PlayerConnection.getMethod("sendPacket", class_Packet);
            method_GetId = class_Entity.getMethod("getId");
            method_GetHandle = class_CraftWorld.getMethod("getHandle");
            method_CraftEntityGetHandle = class_CraftEntity.getMethod("getHandle");
            method_CraftServerGetHandle = class_CraftServer.getMethod("getHandle");
            method_DWA = class_DataWatcher.getMethod("a", int.class, Object.class);
            method_DWB = class_DataWatcher.getMethod("b");
            method_GetBlock = class_CraftMagicNumbers.getMethod("getBlock", Block.class);
            method_SendPacketNearby = class_PlayerList.getMethod("sendPacketNearby", double.class, double.class, double.class, double.class, int.class, class_Packet);
            method_G = class_Block.getMethod("g", class_World, class_BlockPosition);
            method_A = class_ItemStack.getMethod("a", class_Block);
            method_HasTag = class_ItemStack.getMethod("hasTag");
            method_SetTag = class_ItemStack.getMethod("setTag", class_NBTTagCompound);
            method_GetTag = class_ItemStack.getMethod("getTag");
            method_Set = class_NBTTagCompound.getMethod("set", String.class, class_NBTBase);
            method_SetInt = class_NBTTagCompound.getMethod("setInt", String.class, int.class);
            method_GetInt = class_NBTTagCompound.getMethod("getInt", String.class);
            method_HasKey = class_NBTTagCompound.getMethod("hasKey", String.class);
            method_AsCraftMirror = class_CraftItemStack.getMethod("asCraftMirror", class_ItemStack);
        } catch (Throwable ex) {
            ex.printStackTrace();
        }
    }

    private void loadConstructors(){
        try {
            constructor_PacketPlayOutSpawnEntityLiving = class_PacketPlayOutSpawnEntityLiving.getConstructor();
            constructor_PacketPlayOutEntityDestroy = class_PacketPlayOutEntityDestroy.getConstructor(int[].class);
            constructor_PacketPlayOutEntityMetadata = class_PacketPlayOutEntityMetadata.getConstructor();
            constructor_DataWatcher = class_DataWatcher.getConstructor(class_Entity);
            constructor_PacketPlayOutBlockBreakAnimation = class_PacketPlayOutBlockBreakAnimation.getConstructor(int.class, class_BlockPosition, int.class);
            constructor_BlockPosition = class_BlockPosition.getConstructor(int.class, int.class, int.class);
            constructor_NBTTagCompound = class_NBTTagCompound.getConstructor();
            constructor_NBTTagList = class_NBTTagList.getConstructor();
        } catch (Throwable ex) {
            ex.printStackTrace();
        }
    }

    private Class<?> fixBukkitClass(String className) {
        className = className.replace("org.bukkit.craftbukkit.", "org.bukkit.craftbukkit." + versionPrefix);
        className = className.replace("net.minecraft.server.", "net.minecraft.server." + versionPrefix);
        try {
            return Class.forName(className);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }

    public Object getConnection(Player p){
        Object toReturn = null;

        try {
            Method method_GetHandle = p.getClass().getMethod("getHandle");
            Object nmsPlayer = method_GetHandle.invoke(p);
            Field field_PlayerConnection = nmsPlayer.getClass().getField("playerConnection");
            toReturn = field_PlayerConnection.get(nmsPlayer);
        } catch (Exception ignored){}

        return toReturn;
    }

    public Method getMethod_GetInt() {
        return method_GetInt;
    }

    public Method getMethod_HasKey() {
        return method_HasKey;
    }

    public Method getMethod_SetInt() {
        return method_SetInt;
    }

    public Method getMethod_AsCraftMirror() {
        return method_AsCraftMirror;
    }

    public Method getMethod_Set() {
        return method_Set;
    }

    public Constructor<?> getConstructor_NBTTagList() {
        return constructor_NBTTagList;
    }

    public Method getMethod_GetTag() {
        return method_GetTag;
    }

    public Method getMethod_SetTag() {
        return method_SetTag;
    }

    public Method getMethod_HasTag() {
        return method_HasTag;
    }

    public Constructor<?> getConstructor_NBTTagCompound() {
        return constructor_NBTTagCompound;
    }

    public Method getMethod_A() {
        return method_A;
    }

    public Method getMethod_G() {
        return method_G;
    }

    public Method getMethod_GetBlock() {
        return method_GetBlock;
    }

    public Method getMethod_SendPacketNearby() {
        return method_SendPacketNearby;
    }

    public Method getMethod_CraftServerGetHandle() {
        return method_CraftServerGetHandle;
    }

    public Constructor<?> getConstructor_PacketPlayOutBlockBreakAnimation() {
        return constructor_PacketPlayOutBlockBreakAnimation;
    }

    public Constructor<?> getConstructor_BlockPosition() {
        return constructor_BlockPosition;
    }

    public Method getMethod_DWB() {
        return method_DWB;
    }

    public Constructor<?> getConstructor_PacketPlayOutEntityMetadata() {
        return constructor_PacketPlayOutEntityMetadata;
    }

    public Constructor<?> getConstructor_DataWatcher() {
        return constructor_DataWatcher;
    }

    public Method getMethod_DWA() {
        return method_DWA;
    }

    public Method getMethod_CraftEntityGetHandle() {
        return method_CraftEntityGetHandle;
    }

    public Method getMethod_GetHandle() {
        return method_GetHandle;
    }

    public Method getMethod_GetId() {
        return method_GetId;
    }

    public Constructor<?> getConstructor_PacketPlayOutEntityDestroy() {
        return constructor_PacketPlayOutEntityDestroy;
    }

    public Method getMethod_SendPacket() {
        return method_SendPacket;
    }

    public Constructor<?> getConstructor_PacketPlayOutSpawnEntityLiving() {
        return constructor_PacketPlayOutSpawnEntityLiving;
    }

    public Method getMethod_asNMSCopy() {
        return method_asNMSCopy;
    }

}
