package me.zath.skyquarries.utils;
/*
 * MC 
 * Created by zAth
 */

import me.zath.skyquarries.SkyQuarries;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.Vector;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Utils {

    public Boolean isInt(String string) {
        try {
            Integer.parseInt(string);
            return true;
        } catch (NumberFormatException exception) {
            return false;
        }
    }

    public Double getSecondsToBreak(ItemStack tool, Block block) {
        //https://minecraft.gamepedia.com/Breaking#Speed
        if (block == null)
            return Double.MAX_VALUE;

        try {

            Object nmsItem = SkyQuarries.getSkyQuarries().getReflection().getMethod_asNMSCopy().invoke(null, tool);
            Object nmsBlock = SkyQuarries.getSkyQuarries().getReflection().getMethod_GetBlock().invoke(null, block);
            Object blockPosition = SkyQuarries.getSkyQuarries().getReflection().getConstructor_BlockPosition().newInstance(block.getX(), block.getY(), block.getZ());

            float hardness = (float) SkyQuarries.getSkyQuarries().getReflection().getMethod_G().invoke(nmsBlock, SkyQuarries.getSkyQuarries().getReflection().getMethod_GetHandle().invoke(block.getWorld()), blockPosition);
            boolean properTool = SkyQuarries.getSkyQuarries().getToolBreakBlocksController().isProperTool(tool, block);
            boolean canHarverst = !block.getDrops(tool).isEmpty();

            double baseTime = canHarverst ? hardness * 1.5 : hardness * 5;
            double breakSpeed = 1.0; //TODO is it 1? its close, but not exact..

            if (properTool) {
                breakSpeed *= (float) SkyQuarries.getSkyQuarries().getReflection().getMethod_A().invoke(nmsItem, nmsBlock);

                if (tool.getItemMeta().hasEnchants() && tool.getItemMeta().hasEnchant(Enchantment.DIG_SPEED)) {
                    int level = tool.getItemMeta().getEnchantLevel(Enchantment.DIG_SPEED);
                    breakSpeed += Math.pow(level, 2) + 1;
                }
            }

            baseTime /= breakSpeed;

            return baseTime;


        } catch (InstantiationException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }

        return 0.0;
    }

    public String getCardinalDirection(float yaw) {
        double rotation = (yaw - 90) % 360;
        if (rotation < 0) {
            rotation += 360.0;
        }
        if (0.0 <= rotation && rotation < 45.0) {
            return "W";
        } else if (45.0 <= rotation && rotation < 135.0) {
            return "N";
        } else if (135.0 <= rotation && rotation < 225.0) {
            return "E";
        } else if (225.0 <= rotation && rotation < 315.0) {
            return "S";
        } else if (315.0 <= rotation && rotation < 360.0) {
            return "W";
        } else {
            return null;
        }
    }

    public Vector getDirection(String cardinalDirection) {
        switch (cardinalDirection) {
            case "N":
                return new Vector(0, 0, -1);
            case "E":
                return new Vector(1, 0, 0);
            case "S":
                return new Vector(0, 0, 1);
            case "W":
                return new Vector(-1, 0, 0);
            default:
                return new Vector(0, 0, 0);
        }
    }

    public Vector getLeftSide(String cardinalDirection) {
        switch (cardinalDirection) {
            case "N":
                return new Vector(-1, 0, 0);
            case "E":
                return new Vector(0, 0, -1);
            case "S":
                return new Vector(1, 0, 0);
            case "W":
                return new Vector(0, 0, 1);
            default:
                return new Vector(0, 0, 0);
        }
    }

    public Vector getBackSide(String cardinalDirection) {
        switch (cardinalDirection) {
            case "N":
                return new Vector(0, 0, 1);
            case "E":
                return new Vector(-1, 0, 0);
            case "S":
                return new Vector(0, 0, -1);
            case "W":
                return new Vector(1, 0, 0);
            default:
                return new Vector(0, 0, 0);
        }
    }

    public Vector getRighttSide(String cardinalDirection) {
        switch (cardinalDirection) {
            case "N":
                return new Vector(1, 0, 0);
            case "E":
                return new Vector(0, 0, 1);
            case "S":
                return new Vector(-1, 0, 0);
            case "W":
                return new Vector(0, 0, -1);
            default:
                return new Vector(0, 0, 0);
        }
    }

    public Location getCenter(Location location, String direction) {
        if (direction.equals("N")) {
            return location.clone().add(getBackSide(direction).multiply(0.5)).add(getRighttSide(direction).multiply(0.5));
        } else if (direction.equals("E")) {
            return location.clone().add(getDirection(direction).multiply(0.5)).add(getRighttSide(direction).multiply(0.5));
        } else if (direction.equals("S")) {
            return location.clone().add(getDirection(direction).multiply(0.5)).add(getLeftSide(direction).multiply(0.5));
        } else if (direction.equals("W")) {
            return location.clone().add(getBackSide(direction).multiply(0.5)).add(getLeftSide(direction).multiply(0.5));
        }

        return null;
    }

    public List<ItemStack> getFortuneDrops(int enchantmentLevel, List<ItemStack> drops) {
        List<ItemStack> toReturn = new ArrayList<>();

        for (ItemStack itemStack : drops) {
            if (itemStack.getType() == Material.COAL || itemStack.getType() == Material.DIAMOND || itemStack.getType() == Material.EMERALD
                || itemStack.getType() == Material.QUARTZ || (itemStack.getType() == Material.INK_SACK && itemStack.getData().getData() == 4)) {

                if (enchantmentLevel == 1 && getRandomNumber(1, 100) <= 33) {
                    itemStack.setAmount(itemStack.getAmount() * 2);
                } else if (enchantmentLevel == 2 && getRandomNumber(1, 100) <= 50) {
                    if (getRandomNumber(1, 100) <= 50) {
                        itemStack.setAmount(itemStack.getAmount() * 2);
                    } else {
                        itemStack.setAmount(itemStack.getAmount() * 3);
                    }
                } else if (enchantmentLevel == 3 && getRandomNumber(1, 100) <= 60) {
                    if (getRandomNumber(1, 100) <= 33) {
                        itemStack.setAmount(itemStack.getAmount() * 2);
                    } else if (getRandomNumber(1, 100) <= 66) {
                        itemStack.setAmount(itemStack.getAmount() * 3);
                    } else {
                        itemStack.setAmount(itemStack.getAmount() * 4);
                    }
                }
            }
            if (itemStack.getType() == Material.GRAVEL) {
                if (enchantmentLevel == 1 && getRandomNumber(1, 100) <= 14) {
                    toReturn.add(new ItemStack(Material.GRAVEL));
                } else if (enchantmentLevel == 2 && getRandomNumber(1, 100) <= 25) {
                    toReturn.add(new ItemStack(Material.GRAVEL));
                } else if (enchantmentLevel == 3) {
                    toReturn.add(new ItemStack(Material.GRAVEL));
                }
            }
            toReturn.add(itemStack);
        }

        return toReturn;
    }

    public Location deserializeLocation(String serializedLocation) {
        // world:0:0:0
        String[] splited = serializedLocation.split(":");

        World world = SkyQuarries.getSkyQuarries().getServer().getWorld(splited[0]);
        int x = Integer.parseInt(splited[1]);
        int y = Integer.parseInt(splited[2]);
        int z = Integer.parseInt(splited[3]);
        return new Location(world, x, y, z);
    }

    public String serializeLocation(Location location) {
        // world:0:0:0
        return location.getWorld().getName() + ":" + location.getBlockX() + ":" + location.getBlockY() + ":" + location.getBlockZ();
    }

    public Location getPasteLocation(String direction, Location blockLocation, int holeSize) {
        if (direction.equals("N")) {
            return blockLocation.clone()
                .add(getLeftSide(direction)
                    .multiply(Math.floor(SkyQuarries.getSkyQuarries().getSchematicController().getSchematicHashMap().get(holeSize).getLength() / 2))
                    .setY(-2))
                .add(getDirection(direction)
                    .multiply(SkyQuarries.getSkyQuarries().getSchematicController().getSchematicHashMap().get(holeSize).getLength() - 1));
        } else if (direction.equals("E")) {
            return blockLocation.clone()
                .add(getLeftSide(direction)
                    .multiply(Math.floor(SkyQuarries.getSkyQuarries().getSchematicController().getSchematicHashMap().get(holeSize).getLength() / 2)))
                .add(getDirection(direction)
                    .multiply(SkyQuarries.getSkyQuarries().getSchematicController().getSchematicHashMap().get(holeSize).getLength() - 1)
                    .setY(-2));
        } else if (direction.equals("W")) {
            return blockLocation.clone()
                .add(getLeftSide(direction)
                    .multiply(Math.floor(SkyQuarries.getSkyQuarries().getSchematicController().getSchematicHashMap().get(holeSize).getLength() / 2))
                    .setY(-2))
                .add(getDirection(direction)
                    .multiply(SkyQuarries.getSkyQuarries().getSchematicController().getSchematicHashMap().get(holeSize).getLength() - 1));
        } else if (direction.equals("S")) {
            return blockLocation.clone()
                .add(getLeftSide(direction)
                    .multiply(Math.floor(SkyQuarries.getSkyQuarries().getSchematicController().getSchematicHashMap().get(holeSize).getLength() / 2))
                    .setY(-2))
                .add(getDirection(direction)
                    .multiply(SkyQuarries.getSkyQuarries().getSchematicController().getSchematicHashMap().get(holeSize).getLength() - 1));
        } else {
            return null;
        }
    }

    public Integer getRandomNumber(int min, int max) {
        return min + new Random().nextInt(max - min + 1);
    }

}
